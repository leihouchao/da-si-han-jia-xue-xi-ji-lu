from bs4 import BeautifulSoup
from urllib.request import urlopen

html = urlopen("https://morvanzhou.github.io/static/scraping/table.html").read().decode('utf-8')
soup = BeautifulSoup(html, 'html5lib')

# 读取标签的所有子标签
for item in soup.find("table", {"id": "course-list"}).children:
    print(item)

print("-------------------------")
for item in soup.find("table", {"id": "course-list"}).tr.next_siblings:
    print(item)

print("-------------------------")
print(soup.find("img", {"src": "https://morvanzhou.github.io/static/img/course_cover/scraping.jpg"}
                ).parent.previous_sibling.get_text())

