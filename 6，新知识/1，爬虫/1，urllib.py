from urllib.request import urlopen 

html = urlopen(
    "https://morvanzhou.github.io/static/scraping/basic-structure.html"
    ).read().decode("utf-8")

# 读取标题
import re 
res = re.findall(r"<title>(.+?)</title>",html)
print("标题是: ",res[0])
# 读取文章内容
res = re.findall(r"<p>(.+?)</p>",html,flags = re.DOTALL)
print("文章内容是: ",res[0])
# 读取链接
res = re.findall(r'href="(.+?)"',html)
print("链接是: ",res)