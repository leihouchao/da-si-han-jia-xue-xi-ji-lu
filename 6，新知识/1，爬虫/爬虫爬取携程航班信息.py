import urllib.request
import json
import random 
from lxml import etree

# 从搜索中获取rk,CK,r值
def get_param(date,startCity,stopCity):
    url='http://flights.ctrip.com/booking/%s-%s-day-1.html?ddate1=%s'%(startCity,stopCity,date)
    res = urllib.request.urlopen(url).read()
    tree = etree.HTML(res)
    pp=tree.xpath('''//body/script[1]/text()''')[0].split()

    CK_original=pp[3][-34:-2]
    CK=CK_original[0:5]+CK_original[13]+CK_original[5:13]+CK_original[14:]
    rk=pp[-1][18:24]
    num=random.random()*10
    num_str="%.15f"%num
    rk=num_str+rk
    r=pp[-1][27:len(pp[-1])-3]
    return rk,CK,r
# 使用CK,rk,r值搜索数据
def get_ticket(date,rk,CK,r,startCity,stopCity):
    import json
    url='http://flights.ctrip.com/domesticsearch/search/SearchFirstRouteFlights?DCity1=%s&ACity1=%s&SearchType=S&DDate1=%s&IsNearAirportRecommond=0&rk=%s&CK=%s&r=%s'%(startCity,stopCity,date,rk,CK,r)
    headers={
        'Host':"flights.ctrip.com",
        'User-Agent':"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
        'Referer':"http://flights.ctrip.com/booking/hrb-sha-day-1.html?ddate1=2016-05-18"
    }
    headers['Referer']="http://flights.ctrip.com/booking/%s-%s-day-1.html?ddate1=%s"%(startCity,stopCity,date)
   
    res = urllib.request.Request(url,headers=headers)
    res = urllib.request.urlopen(res).read().decode("gb2312")
    dict_content=json.loads(res,encoding="gb2312")
    print (len(dict_content['fis']))
    i = 1
    for jsons in dict_content['fis']:
        start = jsons['dpbn']
        stop = jsons['apbn']
        startDate = jsons['dt']
        print(i,': ',start,'  --->  ',stop,';起飞时间: ',startDate)
        i = i + 1
# 地名和代码的映射
def mapCity2Code(str):
    cityMap = {
        '北京':'PEK',
        '南苑':'NAY',
        '滨海':'TSN',
        '石家庄':'SJW',
        '秦皇岛':'SHP',
        '太原':'TYN',
        '大同':'DTA',
        '长治':'CIH',
        '呼和浩特':'HET',
        '包头':'BAV',
        '乌兰浩特':'HLH',
        '海拉':'HLD',
        '锡林':'XIL',
        '赤峰':'CIF',
        '通辽':'TGO',
        '乌海':'WUA',
        '济南':'TNA',
        '威海':'WEH',
        '青岛':'TAO',
        '潍坊':'WEF',
        '烟台':'YNT',
        '临沂':'LYI',
        '泗水':'SUB',
        '济宁':'TNB',
        '南昌':'KHN',
        '九江':'JIU',
        '景德':'JDZ',
        '赣州':'KOW',
        '黄山':'TXN',
        '合肥':'HFE',
        '安庆':'AQG',
        '阜阳':'FIG',
        '虹桥':'SHA',
        '上海':'SHA',
        '浦东':'PVG',
        '杭州':'HGH',
        '温州':'WNZ',
        '舟山':'HSN',
        '宁波':'NGB',
        '义乌':'YIW',
        '黄岩':'HYN',
        '衢州':'JUZ',
        '无锡':'WUX',
        '南京':'NKG',
        '徐州':'XUZ',
        '连云港':'LYG',
        '盐城':'YNZ',
        '常州':'CZX',
        '南通':'NTG',
        '厦门':'XMN',
        '福州':'FOC',
        '晋江':'JIN',
        '武夷山':'WUS',
        '广州':'CAN',
        '梅州':'MXZ',
        '珠海':'ZUH',
        '汕头':'SWA',
        '深圳':'SZX',
        '湛江':'ZHA',
        '海口':'HAK',
        '三亚':'SYX',
        '郑州':'CGO',
        '洛阳':'LTA',
        '南阳':'NNY',
        '武汉':'WUH',
        '荆州':'SHS',
        '襄樊':'XFN',
        '宜昌':'YIN',
        '恩施':'ENH',
        '张家界':'DYG',
        '长沙':'CSX',
        '常德':'CGD',
        '衡阳':'HNY',
        '南宁':'NNG',
        '桂林':'KWL',
        '北海':'BHY',
        '柳州':'LZH',
        '梧州':'YUZ',
        '成都':'CTU',
        '绵阳':'MIG',
        '宜宾':'YBP',
        '泸州':'LZO',
        '九寨沟':'JAH',
        '攀枝花':'PZI',
        '西昌':'XIC',
        '万县':'WXN',
        '达县':'DAX',
        '南充':'NAO',
        '梁平':'BPX',
        '广汉':'GHX',
        '重庆':'CKG',
        '贵阳':'KWE',
        '铜仁':'TEN',
        '遵义':'ZYI',
        '昆明':'KMG',
        '丽江':'LJG',
        '西双版纳':'JHG',
        '大理':'DLU',
        '芒市':'LUM',
        '迪庆':'DIG',
        '思茅':'SYM',
        '保山':'BSD',
        '昭通':'ZAT',
        '拉萨':'LXA',
        '昌都':'BDU',
        '哈尔滨':'HRB',
        '齐齐哈尔':'NDG',
        '牡丹江':'MDG',
        '黑市':'HEK',
        '佳木斯':'JMU',
        '长春':'CGQ',
        '延吉':'YNJ',
        '吉林':'JIL',
        '大连':'DLC',
        '沈阳':'SHE',
        '绵州':'JNZ',
        '丹东':'DDG',
        '朝阳':'CHG',
        '西安':'SIA',
        '汉中':'HZG',
        '延安':'ENY',
        '安康':'AKA',
        '榆林':'UYN',
        '兰州':'LHW',
        '敦煌':'DNH',
        '嘉峪关':'JGN',
        '庆阳':'IQN',
        '西宁':'XNN',
        '格木尔':'GQQ',
        '银川':'INC',
        '乌鲁木齐':'URC',
        '和田':'HTN',
        '伊宁':'YIN',
        '克拉玛依':'KRY',
        '塔城':'TCG',
        '阿勒泰':'AAT',
        '阿克苏':'AKU',
        '库尔勒':'KRL',
        '库车':'KCA',
        '喀什':'KHG',
        '且末':'IQM',
        '哈密':'NKI',
        '富蕴':'FYN',
    }
    return cityMap[str]
# 读取城市代码
def readCode():
    fp = open('code.txt','rb');
    fw = open('tmp.txt','w');
    data = fp.read().decode('utf-8');
    data = data.split('\n')
    i = 1
    for line in data:
        i = i + 1
        line = line.split('.');
        line_len = len(line);
        for j in range(1,line_len):
            cityName = line[j][0:-3]
            cityCode = line[j][-3:]
            print(cityName,':',cityCode)
            s = '\'' + cityName + '\'' + ':' + '\'' + cityCode + '\''+ '\n';
            fw.write(s)
 
# 主函数
def main():
    startCity = input('请输入开始城市:  ');
    stopCity = input('请输入结束城市:  ');
    date = input('请输入日期(xxxx-xx-xx格式):  ');
    startCity = mapCity2Code(startCity)
    stopCity = mapCity2Code(stopCity)

    print(startCity,stopCity,date)
    rk,CK,r = get_param(date,startCity,stopCity)
    get_ticket(date,rk,CK,r,startCity,stopCity)

main()
