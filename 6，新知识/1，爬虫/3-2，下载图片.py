import os 
os.makedirs("./imgs/",exist_ok = True)

IMAGE_URL = "https://morvanzhou.github.io/static/img/description/learning_step_flowchart.png"

# 使用 urllib 下载
def urllib_download():
    from urllib.request import urlretrieve
    urlretrieve(IMAGE_URL,"./imgs/image1.png")
# 使用 request下载 
def request_download():
    import requests 
    res = requests.get(IMAGE_URL)
    with open("./imgs/image2.jpg",'wb') as f:
        f.write(res.content)
# 分批下载
def chunk_download():
    import requests 
    res = requests.get(IMAGE_URL, stream = True)
    with open("./imgs/image3.jpg",'wb') as f:
        for chunk in res.iter_content(chunk_size = 64):
            f.write(chunk)

urllib_download()
print("下载完成..")
request_download()
print("下载完成...")
chunk_download()
print("下载完成...")