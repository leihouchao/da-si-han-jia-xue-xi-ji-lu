from urllib.request import urlopen 
from bs4 import BeautifulSoup 

html = urlopen(
    "https://morvanzhou.github.io/static/scraping/basic-structure.html"
    ).read().decode("utf-8")

# 使用bs进行解析
data = BeautifulSoup(html,"html5lib")
print("标题是: ",data.h1)
print("内容是: ",data.p)
# 解析链接
all_link = data.find_all('a')
print(all_link)
# 解析链接里面的url 
all_href = [link['href'] for link in all_link]
print("url是: ",all_href)