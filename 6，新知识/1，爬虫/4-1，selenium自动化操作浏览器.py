from selenium import webdriver 
from selenium.webdriver.chrome.options import Options 
import time 

chrome_options = Options() 
driver = webdriver.Chrome(chrome_options = chrome_options)

driver.get("https://morvanzhou.github.io/")
driver.find_element_by_xpath(u"//img[@alt='强化学习 (Reinforcement Learning)']").click()
driver.find_element_by_link_text('About').click()
driver.find_element_by_link_text(u'赞助').click()
driver.find_element_by_link_text(u'教程 ▾').click()
time.sleep(1)
driver.find_element_by_link_text(u'数据处理 ▾').click()
time.sleep(1)
driver.find_element_by_link_text(u'网页爬虫').click()
time.sleep(1)

# 读取网页内容
print(driver.page_source[:200])
driver.get_screenshot_as_file("./imgs/sreenshot2.png")

driver.close()
print('结束...')