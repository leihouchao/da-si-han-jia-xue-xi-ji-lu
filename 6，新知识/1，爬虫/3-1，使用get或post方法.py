import requests 

# get方法
def get():
    print("get方法")
    param = {"wd": "莫烦Python"}
    res = requests.get('http://www.baidu.com/s',params = param)
    print(res.url) 
    # print(res.text)
# post方法: 提交json数据
def post_data():
    print("post方法")
    data = {'firstname': '侯超', 'lastname': '雷'}
    res = requests.post('http://pythonscraping.com/files/processing.php', 
            data = data)
    print(res.text)
# post方法: 提交图片
def post_img():
    print("提交图片")
    file = {'uploadFile':open('./img.jpg','rb')}
    res = requests.post('http://pythonscraping.com/files/processing2.php', files = file)
    print(res.text)
# post方法: 登陆
def post_login():
    print("post登陆")
    data = {'username': '雷后超', 'password': 'password'}
    res = requests.post('http://pythonscraping.com/pages/cookies/welcome.php', data = data)
    print(res.cookies.get_dict())
    res = requests.get('http://pythonscraping.com/pages/cookies/profile.php', cookies=res.cookies)
    print(res.text)
# 使用session进行登陆
def session_login():
    print("使用session进行登陆")
    session = requests.Session() 
    data = {'username': '雷后超', 'password': 'password'}
    res = session.post('http://pythonscraping.com/pages/cookies/welcome.php', data = data)
    print(res.cookies.get_dict())

    res = session.get("http://pythonscraping.com/pages/cookies/profile.php")
    print(res.text)


get()
# post_data()
# post_img()
# post_login()
# session_login()