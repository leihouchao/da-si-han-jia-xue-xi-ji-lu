from bs4 import BeautifulSoup
from urllib.request import urlopen 
import re 

html = urlopen("https://morvanzhou.github.io/static/scraping/table.html").read().decode('utf-8')
soup = BeautifulSoup(html,'html5lib')

# 正则: .*? 和 .+? 代表任意匹配
img_links = soup.find_all('img',{'src':re.compile('.*?\.jpg')})
for link in img_links:
    print("图片的链接是: ",link['src'])

print()

source_link = soup.find_all('a',{'href':re.compile("https://morvan.*")})
for link in source_link:
    print("网页链接是: ",link['href'])