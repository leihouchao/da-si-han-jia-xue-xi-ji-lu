from urllib.request import urlopen
from bs4 import BeautifulSoup 

html = urlopen("https://morvanzhou.github.io/static/scraping/list.html").read().decode('utf-8')

soup = BeautifulSoup(html,"html5lib")

# 解析特定的列表
# (1) 使用find_all读取这一整个标签
months = soup.find_all('li',{'class':'month'})
# (2) 使用['属性']读取标签里面的属性
for m in months:
    # (3) 使用get_text()读取标签里面的内容
    print('他们是内容是: ',m.get_text())

# 继续解析
jan = soup.find_all('ul',{'class':'jan'})
print("jan: ",jan)
jan_li = jan[0].find_all('li')
for li in jan_li:
    print(li.get_text())