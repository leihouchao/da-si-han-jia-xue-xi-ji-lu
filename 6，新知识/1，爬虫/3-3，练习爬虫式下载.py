from bs4 import BeautifulSoup 
import requests 
import os 
os.makedirs("./imgs/",exist_ok = True) 

URL = "http://www.nationalgeographic.com.cn/animals/"

html = requests.get(URL).text 
soup = BeautifulSoup(html,'html5lib')
img_url = soup.find_all('ul',{'class':'img_list'})

for ul in img_url:
    imgs = ul.find_all('img')
    for img in imgs:
        url = img['src']
        res = requests.get(url,stream = True)
        img_name = url.split('/')[-1]
        with open('./imgs/%s' % img_name,'wb') as f:
            for chunk in res.iter_content(chunk_size=128):
                f.write(chunk)
        print(img_name ,' 下载完成')