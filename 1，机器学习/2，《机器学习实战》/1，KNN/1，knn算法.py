from numpy import *
import operator 

# 创建数据
def createData():
    x = array([[1,1],[1,1.2],[0,0],[0,0.2]])
    y = ['a','a','b','b']
    return x,y

# knn 算法
def knn(X,Y,x,k):
    m,n = shape(X)
    # 排序
    diff = tile(x,(m,1)) - X  # 计算差值
    diff = diff ** 2          # 计算平方
    diff = diff.sum(axis = 1) # 计算平方和
    diff = diff.argsort()     # 排序
    # 计算各个类的数量
    classCount = {}
    for i in range(k):
        label = Y[diff[i]]    # 获取类别
        classCount[label] = classCount.get(label,0) + 1;
    # 对classCount排序
    sortCount = sorted(classCount.items(),key = operator.itemgetter(1),reverse=True)
    return sortCount[0][0]

# main函数
def main():
    X,Y = createData()
    print(knn(X,Y,[3.1,3.2],3))

# main()