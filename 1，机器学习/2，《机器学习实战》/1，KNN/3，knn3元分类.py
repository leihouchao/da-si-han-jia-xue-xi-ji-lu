import numpy as np 
from numpy import * 
import operator 
import matplotlib.pyplot as plt 

# 读取文件
def readData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1 
    X = []; Y = [] 
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        X.append(tmp)
        Y.append(int(line[-1]))
    return X,Y 

# 归一化数据
def normData(X):
    X = np.array(X)    
    minVal = X.min(0)
    maxVal = X.max(0)
    ranges = maxVal - minVal 
    
    norms = zeros(shape(X))
    m = shape(X)[0]
    norms = X - tile(minVal,(m,1))
    norms = norms / tile(ranges,(m,1))
    return norms,ranges,minVal

# knn 分类
def knn(X,Y,x,k):
    m,n = shape(X)
    # 排序
    diff = tile(x,(m,1)) - X  # 计算差值
    diff = diff ** 2          # 计算平方
    diff = diff.sum(axis = 1) # 计算平方和
    diff = diff.argsort()     # 排序
    # 计算各个类的数量
    classCount = {}
    for i in range(k):
        label = Y[diff[i]]    # 获取类别
        classCount[label] = classCount.get(label,0) + 1;
    # 对classCount排序
    sortCount = sorted(classCount.items(),key = operator.itemgetter(1),reverse=True)
    return sortCount[0][0]

# 主函数
def run():
    h = 0.1 
    X,y = readData('./data/train.txt')
    norms,_,_ = normData(X)
    m = shape(X)[0]
    error = 0 
    mTest = int(m * h)

    for i in range(mTest):
        res = knn(norms[mTest:m,:],y[mTest:m],norms[i,:],3)
        if(res != y[i]):
            error += 1 
            print("预测出错....")
    print("正确率: ",(1 - float(error) / float(m)) * 100 )

# 及时预测
def predict():
    a = float(input("输入a: "))
    b = float(input("输入b: "))
    c = float(input("输入c: "))
    X,y = readData('./data/train.txt')
    norms,ranges,minVal = normData(X)
    a = array([a,b,c])
    res = knn(norms,y,(a - minVal) / ranges,3)
    print("分类结果是: ",res)

# main
def main():
    # run()
    predict()

main()
