from numpy import * 
import numpy as np 
import operator
from os import listdir 

# knn 算法
def knn(X,Y,x,k):
    m,n = shape(X)
    # 排序
    diff = tile(x,(m,1)) - X 
    diff = diff ** 2
    diff = diff.sum(axis = 1)
    diff = diff.argsort() 
    # 找class
    labels = {}
    for i in range(k):
        label = Y[diff[i]]
        labels[label] = labels.get(label,0) + 1 
    # 对class排序
    sortedLabels = sorted(labels.items(),key = operator.itemgetter(1),reverse=True)
    return  sortedLabels[0][0]

# 读取文件
def img2vec(filename):
    n = len(open(filename).readline().strip())
    X = []
    fp = open(filename)
    for line in fp.readlines():
        tmp = []
        for i in range(n):
            tmp.append(int(line[i]))
        X.append(tmp)
    X = np.reshape(X,(1,1024))
    return X 

# 手写字体分类
def run():
    # 加载训练集
    trainFileList = listdir("./data/train/")
    labels = []
    m = len(trainFileList)
    trainMat = zeros((m,1024))
    for i in range(m):
        # 统计类别
        filename = trainFileList[i]
        tmp = filename.split('.')[0]
        classNum = int(tmp.split('_')[0])
        labels.append(classNum)
        # 转换文件
        trainMat[i,:] = img2vec("./data/train/%s"%filename)

    # 加载测试集
    error = 0.0
    testFileList = listdir("./data/test/")
    mTest = len(testFileList)
    for i in range(mTest):
        # 统计类别
        filename = testFileList[i]
        tmp =filename.split('.')[0]
        ans = int(tmp.split('_')[0])
        # 转换文件
        res = knn(trainMat,labels,img2vec("./data/test/%s"%filename),3)
        if(res != ans):
            print("预测出错,正确的是: ",ans,', 预测的是: ',res)
            error += 1.0 
    print("正确率: ", (1-float(error)/float(mTest))*100,'%')

# main
def main():
    run()

main()
     