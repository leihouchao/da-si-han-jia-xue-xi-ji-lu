from numpy import * 
import numpy as np 

# 读取数据
def loadData(filename):
    lines = open(filename).readlines()
    m = len(lines)
    x = []
    for line in lines:
        line = line.strip().split('\t')
        tmp = []
        tmp.append([float(line[0]),float(line[1])])
        x.append(tmp)
    x = np.reshape(x,(m,2))
    return mat(x)

# pca
def pca(x,n):
    # 取均值
    meanVal = mean(x,axis = 0)
    x = x - meanVal
    # 求协方差矩阵
    covX = x.T * x
    # 求特征值,特征矩阵
    eigVal,eigVec = linalg.eig(mat(covX))
    eigValIdx = argsort(eigVal)
    eigValIdx = eigValIdx[:-(n+1):-1]

    V = eigVec[:,eigValIdx]
    A = x * V 
    X_new = (A * V.T) + meanVal
    return A,X_new

# 画图
def plotLine(x,x_new):
    import matplotlib.pyplot as plt 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x[:,0].flatten().A[0],x[:,1].flatten().A[0],marker = '^', s = 90)
    ax.scatter(x_new[:,0].flatten().A[0],x_new[:,1].flatten().A[0],
                marker = 'o', s = 50, c = 'red')
    plt.show()

x = loadData('testSet.txt')
a,x_new = pca(x,1)
plotLine(x,x_new)