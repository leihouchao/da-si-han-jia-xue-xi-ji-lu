from numpy import * 
import numpy as np 

# 读取数据
def loadData():
    n = len(open('secom.data').readline().strip().split(' '))
    x = []
    fp = open('secom.data')
    for line in fp.readlines():
        line = line.strip().split(' ')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        x.append(tmp)
    x = mat(x)
    for i in range(n):
        meanVal = mean(x[nonzero(~np.isnan(x[:,i].A))[0],i])
        x[nonzero(np.isnan(x[:,i].A))[0],i] = meanVal 
    return x 

# pca 
def pca(x,n):
    # 取均值
    meanVal = mean(x,axis = 0)
    x = x - meanVal
    # 取协方差矩阵
    covX = x.T * x
    # 求特征值,特征矩阵
    eigVal,eigVec = linalg.eig(mat(covX))
    eigValIdx = argsort(eigVal)
    eigValIdx = eigValIdx[:-(n + 1):-1]

    v = eigVec[:,eigValIdx]
    A = x * v 
    x_new = (A * v.T) + meanVal
    return A,x_new 

x = loadData()
A,x_new = pca(x,6)
print(shape(A))
print(shape(x_new))
