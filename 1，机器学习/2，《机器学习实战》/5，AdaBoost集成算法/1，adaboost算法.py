from numpy import * 
import numpy as np 

# 创建数据
def loadsimData():
    data = mat([
        [1.0,2.1],
        [2.0,1.1],
        [1.3,1.0],
        [1.0,1.0],
        [2.0,1.0]
    ])
    labels = [1,1,-1,-1,1]
    return data,labels

# 根据阈值对数据进行分类: 返回分类结果
def stumpClassify(data,dim,threshVal,eq):
    ret = ones((shape(data)[0],1))
    if eq == 'lt':
        ret[data[:,dim] <= threshVal]  = -1
    else:
        ret[data[:,dim] > threshVal]  = -1
    return ret 

# 创建一个弱的分类器
def buildStump(data,labels,D):
    data = mat(data)
    labels = mat(labels).T
    m,n = shape(data)
    numSteps = 10; bestStrump = {}; bestClass = mat(zeros((m,1)))
    minError = inf 

    for i in range(n):
        rangeMin = data[:,i].min();
        rangeMax = data[:,i].max();
        stepSize = (rangeMax - rangeMin) / numSteps
        for j in range(-1,int(numSteps) + 1):
            for eq in ['lt','gt']:
                threshval = (rangeMin + float(j) * stepSize)
                pre = stumpClassify(data,i,threshval,eq)
                err = mat(ones((m,1)))
                err[pre == labels] = 0
                weightError = D.T * err 

                if weightError < minError:
                    minError = weightError
                    bestClass = pre.copy()
                    bestStrump['dim'] = i
                    bestStrump['thresh'] = threshval
                    bestStrump['eq'] = eq
    return bestStrump,minError,bestClass

# adaboost算法
def adaboost(data,labels,Iters = 40):
    weakClassArr = []
    m = shape(data)[0]
    D = mat(ones((m,1))/m)
    aggClass = mat(zeros((m,1)))
    for i in range(Iters):
        # 获取分类器
        bestStrump,error,classEat = buildStump(data,labels,D)
        alpha = float(0.5 * log((1.0 - error) / max(error,1e-16)))
        # 保存分类器
        bestStrump['alpha'] = alpha 
        weakClassArr.append(bestStrump)
        # 利用alpha计算D,更新下一步的计算
        expon = multiply( -1 * alpha * mat(labels).T, classEat)
        D = multiply(D,exp(expon))
        D = D/D.sum() 
        # 计算错误率
        aggClass += alpha * classEat
        aggError = multiply(sign(aggClass) != mat(labels).T, ones((m,1)))
        errorRate = aggError.sum() / m 

        print("错误率: ",errorRate)
        if errorRate == 0: break
    return weakClassArr

# 做预测,分类
def adaClassify(data,classArr):
    data = mat(data)
    m = shape(data)[0]
    aggClass = mat(zeros((m,1)))
    for i in range(len(classArr)):
        classEst = stumpClassify(data,classArr[i]['dim'],classArr[i]['thresh'],classArr[i]['eq'])
        aggClass += classArr[i]['alpha'] * classEst
        # print(aggClass)
    return sign(aggClass)

# main
def main():
    data,labels = loadsimData()
    '''
    D = mat(ones((5,1))/5)
    stump,minError,bestClass = buildStump(data,labels,D)
    print(stump,minError,bestClass)
    '''
    classArr = adaboost(data,labels,9)
    print('--------------------------------------')
    print(adaClassify([0,0],classArr))
    print('--------------------------------------')
    print(adaClassify([[5,5],[0,0]],classArr))

main()