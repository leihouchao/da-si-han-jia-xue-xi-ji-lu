from numpy import *
import numpy as np 

# 加载数据
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) -1 
    data = []
    labels = [] 
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        data.append(tmp)
        labels.append(float(line[-1]))
    return data,labels

# 根据阈值对数据分类
def stumpClassify(data,dim,threshval,eq):
    ret = ones((shape(data)[0],1))
    if eq == 'lt':
        ret[data[:,dim] <= threshval] = -1.0 
    else:
        ret[data[:,dim] > threshval] = -1.0 
    return ret 

# 创建一个弱分类器
def buildStump(data,labels,D):
    data = mat(data); labels = mat(labels).T 
    m,n = shape(data)
    numSteps = 10;
    bestStrump = {}; bestClass = mat(zeros((m,1)))
    minError = inf 

    for i in range(n):
        rangeMin = data[:,i].min(); rangeMax = data[:,i].max();
        stepSize = (rangeMax - rangeMin) / numSteps
        for j in range(-1,int(numSteps) + 1):
            for eq in ['lt','gt']:
                threshval = (rangeMax - rangeMin) / numSteps
                pre = stumpClassify(data,i,threshval,eq)
                err = mat(ones((m,1)))
                err[pre == labels] = 0
                weightError = D.T * err 

                if weightError < minError:
                    minError = weightError
                    bestClass = pre.copy()
                    bestStrump['dim'] = i 
                    bestStrump['thresh'] = threshval
                    bestStrump['eq'] = eq 
    return bestStrump,minError,bestClass

# adaboost算法
def adaboost(data,labels,Iter = 40):
    weakClassArr = []
    m = shape(data)[0]
    D = mat(ones((m,1))/m)
    aggClass = mat(zeros((m,1)))
    for i in range(Iter):
        bestStrump,error,classEst = buildStump(data,labels,D)
        alpha = float(0.5 * log((1.0 - error) / max(error,1e-16)))
        bestStrump['alpha'] = alpha 
        weakClassArr.append(bestStrump)

        expon = multiply(-1 * alpha * mat(labels).T,classEst)
        D = multiply(D,exp(expon))
        D = D/D.sum()

        aggClass += alpha * classEst
        aggError = multiply(sign(aggClass) != mat(labels).T,ones((m,1)))
        errorRate = aggError.sum() / m 

        print("错误率: ",errorRate)
        if errorRate == 0: break
    return weakClassArr,aggClass

# 绘制ROC曲线
def plotROC(pre,labels):
    import matplotlib.pyplot as plt 
    cur = (1,1)
    ySum = 0
    numPos = sum(array(labels) == 1)
    yStep = 1/float(numPos)
    xStep = 1/float(len(labels) - numPos)

    sortedIdx = pre.argsort()
    fig = plt.figure()
    fig.clf()
    ax = fig.add_subplot(111)
    for index in sortedIdx.tolist()[0]:
        if labels[index] == 1:
            delX = 0; delY = yStep 
        else:
            delX = xStep; delY = 0
            ySum += cur[1]
        ax.plot([cur[0],cur[0] - delX],[cur[1],cur[1] - delY],c = 'b')
        cur = (cur[0] - delX,cur[1] - delY)
    ax.plot([0,1],[0,1],'r--')
    ax.axis([0,1,0,1])
    plt.show()

    print('AUC: \n ', (ySum * xStep))

# 主函数
def main():
    data,labels = loadData('train.txt')
    classArr,aggClass = adaboost(data,labels,40)
    plotROC(aggClass.T,labels)

main() 
    