from numpy import * 
import matplotlib.pyplot as plt 

# 读取文件
def readData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1 
    x = []; y = [] 
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        x_sub = [] 
        for i in range(n):
            x_sub.append(float(line[i]))
        x.append(x_sub)
        y.append(float(line[-1]))
    x = mat(x); y = mat(y).T 
    return x,y 

# 局部加权线性回归
def lwlr(point,x,y,k = 1.0):
    x = mat(x); y = mat(y)
    m = shape(x)[0]
    w = mat(eye(m))
    for j in range(m):
        diff = point - x[j,:]
        w[j,j] = exp(diff * diff.T / (-2 * k * k))
    xTx = x.T * w * x 
    if linalg.det(xTx) == 0:
        print('不可逆')
        return 
    ws = xTx.I * x.T * y
    return point * ws 

# 局部加权回归 test
def lwTest(testx,x,y,k = 1.0):
    m = shape(testx)[0]
    yhat = zeros(m)
    for i in range(m):
        yhat[i] = lwlr(testx[i,:],x,y,k)
    return yhat 

# 计算loss
def lossError(y,yhat):
    y = y.flatten().A[0]
    return ((y - yhat)**2).sum()

x,y = readData('data1.txt')
yhat01 = lwTest(x[0:99,:],x[0:99,:],y[0:99,:],0.1)
print(lossError(y[0:99,:],yhat01))

yhat1 = lwTest(x[0:99,:],x[0:99,:],y[0:99,:],1)
print(lossError(y[0:99,:],yhat1))

yhat10 = lwTest(x[0:99,:],x[0:99,:],y[0:99,:],10)
print(lossError(y[0:99,:],yhat10))

print('=================================')

yhat01 = lwTest(x[100:199,:],x[0:99,:],y[0:99,:],0.5)
print(lossError(y[100:199,:],yhat01))

yhat1 = lwTest(x[100:199,:],x[0:99,:],y[0:99,:],1)
print(lossError(y[100:199,:],yhat1))

yhat10 = lwTest(x[100:199,:],x[0:99,:],y[0:99,:],10)
print(lossError(y[100:199,:],yhat10))
