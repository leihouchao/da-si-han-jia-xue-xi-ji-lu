from numpy import * 
import numpy as np 
import matplotlib.pyplot as plt 

# 读取文件
def readData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1 
    x = [];  y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        x_sub = []
        for i in range(n):
            x_sub.append(line[i])
        x.append(x_sub)
        y.append(float(line[-1]))
    x = mat(x)
    y = mat(y).T 
    return x,y 

# 方程法求解w
def equReg(x,y):
    x = mat(x);
    y = mat(y);
    xTx = x.T * x
    if linalg.det(xTx) == 0:
        print('矩阵不可逆')
        return 
    w = xTx.I * x.T * y 
    return w 

# 交叉验证
def crossValidation(x,y,numVal = 10):
    x = mat(x); y = mat(y)
    m = len(y)
    indexList = np.array(range(m))
    errorMat = zeros((numVal,30))
    for i in range(numVal):
        trainX = []; trainY = []
        testX = [];  testY = []
        random.shuffle(indexList)
        for j in range(m):
            if j < m*0.9:
                trainX.append(x[indexList[j]])
                trainY.append(y[indexList[j]])
            else:
                testX.append(x[indexList[j]])
                testY.append(y[indexList[j]])
        m,_,n = shape(trainX)
        trainX = np.reshape(trainX,(m,n))
        trainY = np.reshape(trainY,(m,1))
        
        m,_,n = shape(testX)
        testX = np.reshape(testX,(m,n))

tmpX,y = readData('out.txt')
m,n = shape(tmpX)
x = mat(ones((m,n+1)))
x[:,1:5] = mat(tmpX)
ws = equReg(x,y)
print(ws)
print("测试==============")
print(x[1] * ws,y[1])
