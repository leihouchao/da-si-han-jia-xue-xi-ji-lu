from numpy import * 
import matplotlib.pyplot as plt 

# 读取文件
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1
    x = []; y =  []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        x_sub = []
        for i in range(n):
            x_sub.append(float(line[i]))
        x.append(x_sub)
        y.append(float(line[-1]))
    x = mat(x);  y = mat(y).T
    return x,y 

# 正则化
def L2Reg(x,y,lamda = 0.2):
    x = mat(x); y = mat(y);
    xTx = x.T * x + lamda * eye(shape(x)[1])
    if linalg.det(xTx) == 0:
        print('不可逆')
        return 
    ws = xTx.I * x.T * y 
    return ws 

# 测试
def Test(x,y):
    x = mat(x); y = mat(y)
    # 数据归一化
    xmean = mean(x,0)
    xvar = var(x,0)
    x = (x - xmean) / xvar 
    ymean = mean(y,0)
    y = y - ymean 
    # 计算不同正则系数情况下的参数w
    w = zeros((30,shape(x)[1]))
    for i in range(30):
        w[i,:] = L2Reg(x,y,exp(i-10)).T 
    return w 

x,y = loadData('data1.txt')
w = Test(x,y)

# 绘图
fig = plt.figure() 
ax = fig.add_subplot(111)
ax.plot(w)
plt.show()