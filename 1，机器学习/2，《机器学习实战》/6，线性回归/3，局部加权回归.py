from numpy import * 
import matplotlib.pyplot as plt 

# 读取文件
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1
    x = [];  y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        x_sub = []
        for i in range(n):
            x_sub.append(float(line[i]))
        x.append(x_sub)
        y.append(float(line[-1]))
    x = mat(x)
    y = mat(y).T 
    return x,y 

########################################
# 局部加权回归 计算系数
#    相当于为某一点使用高斯函数计算其最适合的参数w
########################################
def lwlr(point,x,y,k = 1.0):
    x = mat(x)
    y = mat(y)
    m = shape(x)[0]
    ###################################
    # 不同点
    w = mat(eye(m))
    for j in range(m):
        diff = point - x[j,:]
        w[j,j] = exp(diff * diff.T / (-2.0*k*k))
    xTx = x.T * w * x 
    ###################################
    if linalg.det(xTx) == 0:
        print("矩阵不可逆")
        return 
    ws = xTx.I * x.T * w * y 
    point = mat(point)
    return point * ws 

# 测试所有样本
def lwTest(testx,x,y,k = 1.0):
    m = shape(testx)[0]
    yhat = zeros(m)
    for i in range(m):
        yhat[i] = lwlr(testx[i,:],x,y,k)
    return yhat 

# 画图
def draw(x,y,yhat):
    x = mat(x)
    y = mat(y)
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.scatter(x[:,1].flatten().A[0],y[:,0].flatten().A[0])
    
    srtIdx = x[:,1].argsort(0)
    xsort = x[srtIdx][:,0,:]
    ax.plot(xsort[:,1],yhat[srtIdx],'r--')
    plt.show()

x,y = loadData('ex0.txt')
# print(lwlr(x[0,:],x,y,1.0))
yhat = lwTest(x,x,y,0.03)
draw(x,y,yhat)
