from numpy import * 

# 读取文件
def loadFile(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1 
    x = []; y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        x_sub = []
        for j in range(n):
            x_sub.append(float(line[j]))
        x.append(x_sub)
        y.append(float(line[-1]))
    x = mat(x)
    y = mat(y).T 
    return x,y 
# 计算误差
def lossError(y,yhat):
    return ((y - yhat)**2).sum() 

# 前向逐步线性回归
def stepWise(x,y,eps = 0.01,numIter = 100):
    x = mat(x); y = mat(y)
    # 数据归一化
    ymean = mean(y,0)
    y = y - ymean 
    xmean = mean(x,0)
    xvar = var(x,0)
    x = (x - xmean) / xvar 
    # 计算
    m,n = shape(x)
    retmat = zeros((numIter,n))
    w = zeros((n,1))
    for i in range(numIter):
        print(i,w.T)
        lowestError = inf 
        for j in range(n):
            for sign in [-1,1]:
                wTest = w.copy()
                wTest[j] += eps * sign 
                yTest = x * wTest
                loss = lossError(y.A,yTest.A)
                if loss < lowestError:
                    lowestError = loss
                    wMax = wTest 
        w = wMax.copy() 
        retmat[i,:] = w.T 
    return retmat 

x,y = loadFile('data1.txt')
# print(shape(x),shape(y))
w = stepWise(x,y)