from numpy import * 
import matplotlib.pyplot as plt 

# 加载数据
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) - 1 
    data = []; label = [] 
    fr = open(filename)
    for line in fr.readlines():
        linearr = []
        curLine = line.strip().split('\t')
        for i in range(n):
            linearr.append(float(curLine[i]))
        data.append(linearr)
        label.append(float(curLine[-1]))
    return data,label 

# 正规方程求解
def standReg(x,y):
    x = mat(x); 
    y = mat(y).T;
    xTx = x.T * x ;
    if linalg.det(xTx) == 0:
        print("该矩阵不可逆")
        return 
    ws = xTx.I * (x.T * y)
    return ws 

# 画图比较
def draw(x,y,w):
    x = mat(x)
    y = mat(y)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x[:,1].flatten().A[0],y.T[:,0].flatten().A[0])

    x.sort(0)
    yhat = x * w 
    ax.plot(x[:,1],yhat,'r-')
    plt.show()

x,y = loadData('ex0.txt')
ws = standReg(x,y)
print(ws)
draw(x,y,ws)