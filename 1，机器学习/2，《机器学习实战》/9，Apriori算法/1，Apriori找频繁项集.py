from numpy import *

# 创建数据
def loadData():
    return [[1,3,4],[2,3,5],[1,2,3,5],[2,5]]

# 创建集合1
def createC1(data):
    c1 = []
    for trans in data:
        for item in trans:
            if not [item] in c1:
                c1.append([item])
    c1.sort()
    return list(map(frozenset,c1))

# 统计各项的支持度,过滤掉低支持度的项
def scanD(D,ck,minSupport):
    ssCnt = {}
    # (1) 统计ck中各个项的次数
    for tid in D:
        for can in ck:
            if can.issubset(tid):
                if not can in ssCnt: ssCnt[can] = 1
                else: ssCnt[can] += 1
    # (2) 计算支持度,过滤掉支持度低的项
    numItems = float(len(D))
    retList = []
    supportData = {}
    for key in ssCnt:
        support = ssCnt[key] / numItems
        if support >= minSupport:
            retList.insert(0,key)
        supportData[key] = support 
    return retList,supportData

# 将过滤掉的项组合为新的集合
def aprioriGen(Lk,k):
    retList = []
    lenLk = len(Lk)
    for i in range(lenLk):
        for j in range(i+1,lenLk):
            L1 = list(Lk[i])[:k-2]
            L2 = list(Lk[j])[:k-2]
            if L1 == L2:
                retList.append(Lk[i] | Lk[j])
    return retList 

# apriori算法
def apriori(data,minSupport = 0.5):
    C1 = createC1(data)
    D = list(map(set,data))
    L1,supportData = scanD(D,C1,minSupport)
    L = [L1]
    k = 2
    while len(L[k-2]) > 0:
        Ck = aprioriGen(L[k-2],k)
        Lk,supK = scanD(D,Ck,minSupport)
        supportData.update(supK)
        L.append(Lk)
        k += 1
    return L,supportData

data = loadData()

# C1 = createC1(data)
# D = list(map(set,data))
# L1,supportData = scanD(D,C1,0.5)
# print(L1)

L,supportData = apriori(data,0.7)
print(L)
print(supportData)