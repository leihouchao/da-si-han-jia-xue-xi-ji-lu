################## 构建树 ###########################################
# 定义fp树结构
class treeNode:
    def __init__(self,nameValue,numOccur,parentNode):
        self.name = nameValue
        self.count = numOccur 
        self.nodeLink = None 
        self.parent = parentNode
        self.children = {}
    
    def inc(self,numOccur):
        self.count += numOccur 
    
    def disp(self,ind = 1):
        print(' ' * ind, self.name, ' ',self.count)
        for child in self.children.values():
            child.disp(ind + 1)
    
# 构建FP树
def createTree(data,minSup = 1):
    headerTable = {}
    # 统计每个字符出现的次数
    for tran in data:
        for item in tran:
            headerTable[item] = headerTable.get(item,0) + data[tran]
    # 过滤掉次数较少的字符
    for k in list(headerTable.keys()):
        if headerTable[k] < minSup:
            del(headerTable[k])
    # 如果没有元素项满足要求,则退出
    freqItemSet = set(headerTable.keys())
    if len(freqItemSet) == 0: return None,None
    # 建立头指针表
    for k in headerTable:
        headerTable[k] = [headerTable[k],None] 
    # 开始建立树
    retTree = treeNode('Null Set',1,None)
    for tran,count in data.items():
        localD = {}
        # 对每一个事务进行过滤,滤掉出现个数少于minSup的
        for item in tran:
            if item in freqItemSet:
                localD[item] = headerTable[item][0]
        if len(localD) > 0:
            # 对过滤过的事务进行排序
            orderItem = [v[0] for v in sorted(localD.items(),key = lambda p:p[1],reverse=True)]
            # 使用排序过的事务来生成FP树
            updateTree(orderItem,retTree,headerTable,count)
    return retTree,headerTable

# 使用过滤过的项集建立树
def updateTree(item,inTree,headerTable,count):
    if item[0] in inTree.children:
        inTree.children[item[0]].inc(count)
    else:
        inTree.children[item[0]] = treeNode(item[0],count,inTree)
        if headerTable[item[0]][1] == None:
            headerTable[item[0]][1] = inTree.children[item[0]]
        else:
            updateHeader(headerTable[item[0]][1],inTree.children[item[0]])
    if len(item) > 1:
        updateTree(item[1::],inTree.children[item[0]],headerTable,count)
    
# 更新头指针表的信息
def updateHeader(nodeTree,target):
    while nodeTree.nodeLink != None:
        nodeTree = nodeTree.nodeLink 
    nodeTree.nodeLink = target 

################## 加载数据 ############################################
# 加载数据
def loadData():
    return [
            ['r','z','h','j','p'],
            ['z','y','x','w','v','u','t','s'],
            ['z'],
            ['r','x','n','o','s'],
            ['y','r','x','z','q','t','p'],
            ['y','z','x','e','q','s','t','m']
    ]

# 数据格式转换
def createInitSet(data):
    retDict = {}
    for tran in data:
        retDict[frozenset(tran)] = 1
    return retDict 

################## 寻找频繁项集 ##########################################
# 寻找条件模式基
def ascendTree(leafNode,prefix):
    if leafNode.parent != None:
        prefix.append(leafNode.name)
        ascendTree(leafNode.parent,prefix)
# 寻找某个字母的所有路径
def findPrefixPath(basePat,treeNode):
    condPats = {}
    while treeNode != None:
        prefix = [] 
        ascendTree(treeNode,prefix)
        if len(prefix) > 1:
            condPats[frozenset(prefix[1:])] = treeNode.count 
        treeNode = treeNode.nodeLink 
    return condPats 
# 递归查找频繁项集 
def mineTree(inTree,headerTable,minSup,prefix,freqItemSet):
    bigL = [v[0] for v in headerTable.items()]
    for basePat in bigL:
        newFeat = prefix.copy()
        newFeat.add(basePat)
        freqItemSet.append(newFeat)
        condPats = findPrefixPath(basePat,headerTable[basePat][1])
        myCondTree,myHead = createTree(condPats,minSup)
        if myHead != None:
            print(newFeat,' 的条件树')
            myCondTree.disp(1)
            mineTree(myCondTree,myHead,minSup,newFeat,freqItemSet)

def main():
    rootNode = treeNode('pyramind',9,None)
    rootNode.children['eye'] = treeNode('eye',12,None)
    rootNode.children['eye2'] = treeNode('eye2',4,None)
    rootNode.disp()

    print('==============================================')
    simData = loadData()
    initSet = createInitSet(simData)
    myFPtree,myHeaderTab = createTree(initSet,3)
    myFPtree.disp()

    print('==============================================')
    print(findPrefixPath('x',myHeaderTab['x'][1]))
    print(findPrefixPath('z',myHeaderTab['z'][1]))
    print(findPrefixPath('r',myHeaderTab['r'][1]))

    print('========================================')
    freqItems = []
    mineTree(myFPtree,myHeaderTab,3,set([]),freqItems)
    print(freqItems)
main()