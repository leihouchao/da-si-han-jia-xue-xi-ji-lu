import matplotlib.pyplot as plt 
from math import log 
import operator 
import numpy as np 

#########################################################################
# 画树
# 定义全局变量
decisionNode = dict(boxstyle = 'sawtooth',fc = "0.8")
leafNode = dict(boxstyle = "round4",fc = "0.8")
arrow_args = dict(arrowstyle = "<-")

# 画一个结点
def plotNode(nodeText,centerPt,parentPt,nodeType):
    createPlot.ax1.annotate(nodeText,xy = parentPt,xycoords = 'axes fraction',xytext = centerPt, \
        textcoords = 'axes fraction',va = "center",ha = "center",bbox = nodeType,arrowprops = arrow_args)

# 获取叶子结点数目
def  getNumLeafs(myTree):
    numLeafs = 0
    firstStr = list(myTree.keys())[0]
    secondDict = myTree[firstStr]
    for key in secondDict.keys():
        if type(secondDict[key]).__name__ == 'dict':
            numLeafs += getNumLeafs(secondDict[key])
        else: numLeafs += 1
    return numLeafs

# 获取树的深度
def getTreeDepth(myTree):
    maxDepth = 0
    firstStr = list(myTree.keys())[0]
    secondDict = myTree[firstStr]
    for key in secondDict.keys():
        if type(secondDict[key]).__name__ == 'dict':
            thisDepth = 1 + getTreeDepth(secondDict[key])
        else: thisDepth = 1
        if thisDepth > maxDepth: maxDepth = thisDepth
    return maxDepth

# 在父子结点之间填充文本
def plotMidText(cntPt,parentPt,txt):
    Xmid = (parentPt[0] - cntPt[0]) / 2.0 + cntPt[0]
    Ymid = (parentPt[1] - cntPt[1]) / 2.0 + cntPt[1]
    createPlot.ax1.text(Xmid,Ymid,txt)

# 画树
def plotTree(myTree, parentPt, nodeTxt):#if the first key tells you what feat was split on
    numLeafs = getNumLeafs(myTree)  #this determines the x width of this tree
    depth = getTreeDepth(myTree)
    firstStr = list(myTree.keys())[0]     #the text label for this node should be this
    cntrPt = (plotTree.xOff + (1.0 + float(numLeafs))/2.0/plotTree.totalW, plotTree.yOff)
    plotMidText(cntrPt, parentPt, nodeTxt)
    plotNode(firstStr, cntrPt, parentPt, decisionNode)
    secondDict = myTree[firstStr]
    plotTree.yOff = plotTree.yOff - 1.0/plotTree.totalD
    for key in secondDict.keys():
        if type(secondDict[key]).__name__=='dict':#test to see if the nodes are dictonaires, if not they are leaf nodes   
            plotTree(secondDict[key],cntrPt,str(key))        #recursion
        else:   #it's a leaf node print the leaf node
            plotTree.xOff = plotTree.xOff + 1.0/plotTree.totalW
            plotNode(secondDict[key], (plotTree.xOff, plotTree.yOff), cntrPt, leafNode)
            plotMidText((plotTree.xOff, plotTree.yOff), cntrPt, str(key))
    plotTree.yOff = plotTree.yOff + 1.0/plotTree.totalD

def createPlot(inTree):
    fig = plt.figure(1, facecolor='white')
    fig.clf()
    axprops = dict(xticks=[], yticks=[])
    createPlot.ax1 = plt.subplot(111, frameon=False, **axprops)    #no ticks
    plotTree.totalW = float(getNumLeafs(inTree))
    plotTree.totalD = float(getTreeDepth(inTree))
    plotTree.xOff = -0.5/plotTree.totalW; plotTree.yOff = 1.0;
    plotTree(inTree, (0.5,1.0), '')
    plt.show()

#######################################################################
# 创建树
# 计算熵
def calH(data):
    m = len(data)
    labels = {}
    for featVec in data:
        label = featVec[-1]
        if label not in labels.keys():
            labels[label] = 0
        labels[label] += 1
    H = 0
    for key in labels:
        p = float(labels[key]) / m
        H -= p*log(p,2)
    return H 

# 划分数据集
def splitData(data,axis,value):
    ret = []
    for feat in data:
        if feat[axis] == value:
            reduceData = feat[:axis]
            reduceData.extend(feat[axis+1:])
            ret.append(reduceData)
    return ret

# 选择最好的数据集划分方式
def chooseBestSplit(data):
    n = np.shape(data)[1] - 1
    base = calH(data)
    bestGain = 0; bestFeature = -1
    for i in range(n):
        featList = [example[i] for example in data]
        uniqueValues = set(featList)
        newEntropy = 0
        for value in uniqueValues:
            subData = splitData(data,i,value)
            p = len(subData) / float(len(data))
            newEntropy += p * calH(subData)
        infoGain = base - newEntropy
        if (infoGain > bestGain):
            bestGain = infoGain
            bestFeature = i 
    return bestFeature

# 投票法
def majorCnt(classList):
    classCount = {}
    for vote in classList:
        if vote not in classCount.keys(): classCount[vote] = 0
        classCount[vote] += 1
    sortCount = sorted(classCount.items(),key = operator.itemgetter(1),reverse=True)
    return  sortCount[0][0]

# 创建决策树
def createTree(data,labels):
    classList = [example[-1] for example in data]
    # 该数据集类别完全相同,停止划分
    if classList.count(classList[0]) == len(classList):
        return classList[0]
    # 特征用完,停止划分
    if len(data[0]) == 1:
        return majorCnt(classList)
    # 否则,继续划分
    bestFeat = chooseBestSplit(data)
    bestFeatLabel = labels[bestFeat]
    myTree = {bestFeatLabel:{}}
    del(labels[bestFeat])
    featValue = [example[bestFeat] for example in data]
    uniqueValues = set(featValue)
    for value in uniqueValues:
        sublabels = labels[:]
        myTree[bestFeatLabel][value] = createTree(splitData(data,bestFeat,value),sublabels)
    return myTree

###############################################################
# 主函数
def main():
    fr = open('lenses.txt')
    data = [inst.strip().split('\t') for inst in fr.readlines()]
    labels = ['age','sex','height','width']
    tree = createTree(data,labels)
    createPlot(tree)

main()