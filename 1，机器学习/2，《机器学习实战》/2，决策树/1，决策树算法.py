from math import log 
import operator 
import numpy as np 

#######################################################################
# 创建树
# 计算熵
def calH(data):
    m = len(data)
    labels = {}
    # 统计各个类别的数量
    for featVec in data:
        label = featVec[-1]
        if label not in labels.keys():
            labels[label] = 0
        labels[label] += 1
    H = 0
    for key in labels:
        p = float(labels[key]) / m  # 计算各个类别的概率
        H -= p * log(p,2)           # 计算熵
    return H 

# 划分数据集: 根据某个feature的某个值作为阈值进行划分
def splitData(data,axis,value):
    ret = [] 
    for feat in data:
        if feat[axis] == value:
            reduceData = feat[:axis]
            reduceData.extend(feat[axis+1:])
            ret.append(reduceData)
    return ret 

# 选择最优的数据集划分方式
def chooseBestSplit(data):
    n = np.shape(data)[1] - 1 
    base = calH(data)
    bestGain = 0;
    bestFeature = -1

    for i in range(n):
        featList = [example[i] for example in data]
        uniqueValue = set(featList)
        newEntropy = 0 
        for value in uniqueValue:
            subData = splitData(data,i,value)
            p = len(subData) / float(len(data))
            newEntropy += p * calH(subData)
        infoGain = base - newEntropy

        if (infoGain > bestGain):
            bestGain = infoGain
            bestFeature = i 
    return bestFeature

# 投票法
def majorCnt(classList):
    classCount = {}
    for vote in classList:
        if vote not in classCount.keys():
            classCount[vote] = 0
        classCount[vote] += 1
    sortedCount = sorted(classCount.items(),key = classCount.itemgetter(1),reverse=True)
    return sortedCount[0][0]

# 创建决策树
def createTree(data,labels):
    classList = [example[-1] for example in data]
    # 如果该数据集的类别完全相同,停止划分
    if classList.count(classList[0]) == len(classList):
        return classList[0]
    # 如果特征用完,停止划分
    if len(data[0]) == 1:
        return majorCnt(classList) 
    # 否则,继续划分
    bestFeat = chooseBestSplit(data)
    bestFeatLabel = labels[bestFeat]
    myTree = {bestFeatLabel:{}}
    del(labels[bestFeat])
    
    featValue = [example[bestFeat] for example in data]
    uniqueValues = set(featValue)
    for value in uniqueValues:
        sublabels = labels[:]
        myTree[bestFeatLabel][value] = createTree(splitData(data,bestFeat,value),sublabels)
    return myTree

#######################################################################
# 使用决策树进行分类
def classify(inputTree,labels,testVec):
    firstStr = list(inputTree.keys())[0]
    secondDict = inputTree[firstStr]

    featIndex = labels.index(firstStr)
    for key in secondDict.keys():
        if testVec[featIndex] == key:
            if type(secondDict[key]).__name__ == 'dict':
                classLabel = classify(secondDict[key],labels,testVec)
            else:
                classLabel = secondDict[key]
    return classLabel

# 创建数据
def createData():
    data = [
        [1,1,'yes'],
        [1,1,'yes'],
        [1,0,'no'],
        [0,1,'no'],
        [0,1,'no']
    ]
    labels = ['no surfing','flippers']
    return data,labels 

# 创造数据
def retriveTree(i):
    listOfTrees = [
                {'no surfing':{0:"no",1:{'flippers':{0:"no",1:"yes"}}}},
                {'no surfing':{0:"no",1:{'flippers':{0:{'head':{0:'no',1:'yes'}},1:'no'}}}}
                    ]
    return listOfTrees[i]

#######################################################################
# 存储决策树
def storeTree(tree,filename):
    import pickle
    fp = open(filename,'wb')
    pickle.dump(tree,fp)
    fp.close()

# 读取决策树
def restoreTree(filename):
    import pickle
    fr = open(filename,'rb')
    return pickle.load(fr)

def main():
    myDat,labels = createData()
    '''
    print(myDat)
    print(calH(myDat))
    print(splitData(myDat,1,0))
    print(splitData(myDat,0,0))
    bestFeature = chooseBestSplit(myDat)
    print(bestFeature)
    '''
    myTree = retriveTree(0)
    # print(classify(myTree,labels,[1,0]))
    # print(classify(myTree,labels,[1,1]))
    # 存储树
    storeTree(myTree,'tree.txt')
    print(restoreTree('tree.txt'))

main()
