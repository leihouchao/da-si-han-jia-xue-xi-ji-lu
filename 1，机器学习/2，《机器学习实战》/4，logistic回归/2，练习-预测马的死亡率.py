from numpy import * 
import numpy as np 

# sigmoid函数
def sigmoid(z):
    return 1.0 / (1 + exp(-z))

# 改进的随机梯度下降
def advancedStocGradDescent(x,y):
    m,n = shape(x)
    w = ones(n)
    for j in range(500):
        dataIndex = np.array(range(m)).tolist()
        for i in range(m):
            alpha = 4 / (1.0 + i + j) + 0.0001 
            randIdx = int(random.uniform(0,len(dataIndex)))
            h = sigmoid(sum(x[randIdx] * w))
            error = y[randIdx] - h
            w =  w + alpha * error  * x[randIdx]
            del(dataIndex[randIdx])
    return w

# 分类器
def classifier(x,w):
    p = sigmoid(sum(x*w))
    if p > 0.5: return 1
    else: return 0 
    
# 测试函数
def Test():
    fpTrain = open('train.txt')
    fpTest  = open('test.txt')

    # 训练,计算w
    trainX = [] 
    trainY = []
    for line in fpTrain.readlines():
        line = line.strip().split('\t')
        x_sub = []
        for i in range(21):
            x_sub.append(float(line[i]))
        trainX.append(x_sub)
        trainY.append(float(line[-1]))
    w = advancedStocGradDescent(array(trainX),trainY)

    # 利用w做预测,计算正确率
    errorCount = 0;
    totalCount = 0;
    for line in fpTest.readlines():
        totalCount += 1
        line = line.strip().split('\t')
        x_sub = []
        for i in range(21):
            x_sub.append(float(line[i]))
        if int(classifier(array(x_sub),w) != int(line[-1])):
            errorCount += 1
    errorRate = float(errorCount) / totalCount
    print("错误率是: ",errorRate)
    return errorRate

# 多次测试,求平均
def multiTest():
    num = 10; error = 0
    for k in range(num):
        error += Test()
    print("平均错误率是: ",float(error)/float(num))

multiTest()