from numpy import *
import numpy as np 

# 读取数据
def loadData():
    x = []
    y = [] 
    fp = open('testSet.txt')
    for line in fp.readlines():
        line = line.strip().split('\t')
        x.append([1,float(line[0]),float(line[1])])
        y.append(int(line[2]))
    return mat(x), mat(y).T 

# sigmoid函数
def sigmoid(z):
    return 1.0 / (1 + exp(-z))

##################################################################
# 梯度下降算法
def gradientDescent(x,y):
    x = mat(x); y = mat(y)
    m,n = shape(x)
    alpha = 0.01 
    Iter = 500 
    w = ones((n,1))
    for k in range(Iter):
        h = sigmoid(x*w)
        error = h - y 
        w = w - alpha * x.T * error 
    return w 
# 随机梯度下降算法
def stocGradDescent(x,y):
    m,n = shape(x)
    alpha = 0.01 
    weight = ones(n)
    for i in range(m):
        h = sigmoid(sum(x[i] * weight))
        error = h - y[i]
        weight = weight - alpha * error[0,0] * x[i]
    return weight
# 改进的随机梯度下降
def advancedStocGradDescent(x,y):
    m,n = shape(x)
    w = ones(n)
    for j in range(150):
        dataIndex = np.array(range(m)).tolist()
        for i in range(m):
            alpha = 4 / (1.0 + i + j) + 0.0001 
            randIdx = int(random.uniform(0,len(dataIndex)))
            h = sigmoid(sum(x[randIdx] * w))
            error = y[randIdx] - h
            w =  w + alpha * error * x[randIdx]
            del(dataIndex[randIdx])
    return w
##################################################################
# 画决策边界图
def plotBestFit(w):
    import matplotlib.pyplot as plt 
    x,y = loadData()
    X_array = array(x)
    n = shape(x)[0]
    x1 = []; y1 = []
    x2 = []; y2 = []
    for i in range(n):
        if int(y[i]) == 1:
            x1.append(X_array[i,1])
            y1.append(X_array[i,2])
        else:
            x2.append(X_array[i,1])
            y2.append(X_array[i,2])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x1,y1,c = 'red',marker = 's')
    ax.scatter(x2,y2,c = 'green')
    x = arange(-3,3,0.1)
    y3 = -(w[0] + w[1] * x) / w[2]
    y3 = y3.T 
    
    ax.plot(x,y3)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.show()


def main():
    x,y = loadData()
    # 梯度下降
    w = gradientDescent(x,y)
    print(w)
    plotBestFit(w)

    # 随机梯度下降
    w = stocGradDescent(array(x),y)
    print(w)
    plotBestFit(w)

    # 改进版随机梯度下降
    w = advancedStocGradDescent(array(x),y)
    print(w)
    plotBestFit(w)


main()