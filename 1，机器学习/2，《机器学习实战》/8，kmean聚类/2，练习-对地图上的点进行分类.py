from numpy import *
import numpy as np 

# 读取数据
def loadData():
    x = []
    fp = open('places.txt')
    for line in fp.readlines():
        line = line.strip().split('\t')
        data = []
        data.append([float(line[3]),float(line[4])])
        x.append(data)
    return x 

# 计算距离
def dist(a,b):
    a = np.array(a)
    b = np.array(b)
    aa = sin(a[0][1] * pi / 180) * sin(b[0][1]*pi/180)
    bb = cos(a[0][1]*pi/180) * cos(b[0][1]*pi/180) * cos(pi*(b[0][0]-a[0][0])/180)
    return arccos(aa + bb)*6371.0 

# 随机初始化中心点
def randCent(x,k):
    x = mat(x)
    n = shape(x)[1]
    centers = mat(zeros((k,n)))
    for j in range(n):
        minJ = min(x[:,j])
        rangeJ = float(max(x[:,j] - minJ))
        centers[:,j] = minJ + rangeJ * random.rand(k,1)
    return centers 

# kmean算法
def kmean(x,k):
    x = mat(x); m = shape(x)[0]
    clusters = mat(zeros((m,2)))
    centers = randCent(x,k)

    flag = True 
    while flag:
        flag = False
        for i in range(m):
            minDist = inf; minIndex = -1 
            for j in range(k):
                distJI = dist(centers[j,:],x[i,:])
                if distJI < minDist:
                    minDist = distJI
                    minIndex = j 
            if clusters[i,0] != minIndex: flag = True
            clusters[i,:] = minIndex, minDist**2 
        # 重新计算中心点
        for cent in range(k):
            pts = x[nonzero(clusters[:,0].A == cent)[0]]
            centers[cent,:] = mean(pts,axis = 0)
    return centers,clusters 

# 二分聚类
def bikmean(x,k):
    x = mat(x); m = shape(x)[0]
    cluster = mat(zeros((m,2)))
    center0 = mean(x,axis = 0).tolist()
    cenList = [center0]

    for j in range(m):
        cluster[j,1] = dist(mat(center0),x[j,:])**2 
    while len(cenList) < k:
        lowestSSE = inf 
        for i in range(len(cenList)):
            pts = x[nonzero(cluster[:,0].A == i)[0],:]
            tmp_center,tmp_cluster = kmean(pts,2)
            sseSplit = sum(tmp_cluster[:,1])
            sseNoSplot = sum(cluster[nonzero(cluster[:,0].A != i)[0],1])
            if sseNoSplot + sseSplit < lowestSSE:
                bestCentToSplit = i 
                bestNewCent = tmp_center 
                bestCluster = tmp_cluster 
                lowestSSE = sseNoSplot + sseSplit 
        bestCluster[nonzero(bestCluster[:,0].A == 1)[0],0] = len(cenList)
        bestCluster[nonzero(bestCluster[:,0].A == 0)[0],0] = bestCentToSplit
        cenList[bestCentToSplit] = bestNewCent[0,:]
        cenList.append(bestNewCent[1,:])
        cluster[nonzero(cluster[:,0].A == bestCentToSplit)[0],:] = bestCluster 
    m,_,n = shape(cenList)
    cenList = np.reshape(cenList,(m,n))
    return cenList,cluster 

# 画图显示聚类
def plotCluster(x,center):
    import matplotlib.pyplot as plt 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = np.array(x)
    center = np.array(center)
    x1 = x[:,0]; y1 = x[:,1]
    ax.scatter(x1,y1)
    centerX = center[:,0]; centerY = center[:,1]
    ax.scatter(centerX,centerY,c = 'red')
    plt.show()

# 
x = loadData()       
m,_,n = shape(x)
x = np.reshape(x,(m,n))

centers = randCent(x,4)
plotCluster(x,centers)
centers,_ = kmean(x,3)
plotCluster(x,centers)
centers,_ = bikmean(x,3)
plotCluster(x,centers)


