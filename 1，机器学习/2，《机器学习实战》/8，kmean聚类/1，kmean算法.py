from numpy import * 
import numpy as np 

# 读取数据
def loadData(filename):
    x = []
    n = len(open(filename).readline().strip().split('\t'))
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        data = []
        for i in range(n):
            data.append(float(line[i]))
        x.append(data)
    return x 

# 计算距离
def dist(a,b):
    a = np.array(a)
    b = np.array(b)
    return sqrt(sum(a-b)**2)

# 随机初始化中心点
def randCent(x,k):
    x = mat(x)
    n = shape(x)[1]
    centers = mat(zeros((k,n)))
    for j in range(n):
        minJ = min(x[:,j])
        rangeJ = float(max(x[:,j]) - minJ)
        centers[:,j] = minJ + rangeJ * random.rand(k,1)
    return centers 

# kmean算法
def kmean(x,k):
    x = mat(x)
    m = shape(x)[0]
    cluster = mat(zeros((m,2)))
    centers=  randCent(x,k)
    flag = True 
    while flag:
        flag = False 
        # (1) 为每个样本分配中心点
        for i in range(m):
            minDist = inf; minIndex = -1 
            for j in range(k):
                distJI = dist(centers[j,:],x[i,:])
                if distJI < minDist:
                    minDist = distJI 
                    minIndex = j 
            # 前后2次中心点有移动,flag置true
            if cluster[i,0] != minIndex : flag = True 
            # 赋值
            cluster[i,:] = minIndex,minDist**2 
        # (2) 更新中心点
        for cent in range(k):
            # 计算属于cent的索引
            pts = x[nonzero(cluster[:,0].A == cent)[0]]
            # 计算中心点
            centers[cent,:] = mean(pts,axis = 0)
    return centers,cluster 

# 二分k均值聚类
def bikmean(x,k):
    x = mat(x)
    m = shape(x)[0]
    cluster = mat(zeros((m,2)))
    # 中心点集合
    center0 = mean(x,axis = 0).tolist()
    cenList = [center0]

    for j in range(m):
        cluster[j,1] = dist(mat(center0),x[j,:])**2
    while (len(cenList) < k):
        lowestSSE = inf 
        # 找到一个最优的中心点拆分
        for i in range(len(cenList)):
            pts = x[nonzero(cluster[:,0].A == i)[0],:]
            tmp_center,tmp_cluster = kmean(pts,2)
            sseSplit = sum(tmp_cluster[:,1])
            sseNoSplit = sum(cluster[nonzero(cluster[:,0].A != i)[0],1])
            if (sseSplit + sseNoSplit) < lowestSSE:
                bestCentTpSplit = i 
                bestNewCents = tmp_center 
                bestCluster = tmp_cluster.copy() 
                lowestSSE = sseNoSplit + sseSplit 
        bestCluster[nonzero(bestCluster[:,0].A == 1)[0],0] = len(cenList)
        bestCluster[nonzero(bestCluster[:,0].A == 0)[0],0] = bestCentTpSplit 
        cenList[bestCentTpSplit] = bestNewCents[0,:]
        cenList.append(bestNewCents[1,:])
        cluster[nonzero(cluster[:,0].A == bestCentTpSplit)[0],:] = bestCluster 
    m,_,n = shape(cenList)
    cenList = np.reshape(cenList,(m,n))
    return (cenList),cluster 

# 画图显示聚类
def plotCluster(x,center):
    import matplotlib.pyplot as plt 
    fig = plt.figure() 
    ax = fig.add_subplot(111)

    x = np.array(x)
    center = np.array(center)

    x1 = x[:,0]
    y1 = x[:,1]
    ax.scatter(x1,y1)
    centerX = center[:,0]
    centerY = center[:,1]
    ax.scatter(centerX,centerY,c = 'red')
    
    plt.show()

x = loadData('testSet2.txt')
centers = randCent(x,4)
plotCluster(x,centers)

centers,_ = kmean(x,3)
plotCluster(x,centers)

centers,_ = bikmean(x,3)
plotCluster(x,centers)