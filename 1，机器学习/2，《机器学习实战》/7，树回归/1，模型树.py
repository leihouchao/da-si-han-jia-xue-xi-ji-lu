from numpy import * 
import numpy as np 

# 加载文件
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) 
    data = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        data.append(tmp)
    return data  

# 分割数据集
def splitData(data,feat,value):
    mat0 = data[nonzero(data[:,feat] > value)[0],:]
    mat1 = data[nonzero(data[:,feat] <= value)[0],:]
    return mat0,mat1 

# 线性回归求解
def linearSolve(data):
    m,n = shape(data)
    x = mat(ones((m,n)))
    y = mat(ones((m,1)))
    x[:,1:n] = data[:,0:n-1]
    y = data[:,-1]
    xTx = x.T * x ;
    if linalg.det(xTx) == 0:
        print("矩阵不可逆")
        return
    ws = xTx.I * x.T * y 
    return ws,x,y 

# 模型树的树叶
def modelLeaf(data):
    ws,x,y = linearSolve(data)
    return ws 
# 模型误差
def modelErr(data):
    ws,x,y = linearSolve(data)
    yHat = x * ws 
    return sum(power(yHat - y,2))

# 选择分割点
def chooseBestSplit(data,leafType,errType,ops = (1,4)):
    tolS = ops[0]
    tolN = ops[1]
    # 数据属于同一类了,直接结束划分
    if len(set(data[:,-1].T.tolist()[0])) == 1:
        return None,leafType(data)
    m,n = shape(data)
    s = errType(data)
    bestS = inf; bestIndex = 0; bestValue = 0
    for feat in range(n-1):
        for val in set(data[:,feat].T.tolist()[0]):
            mat0,mat1 = splitData(data,feat,val)
            # 如果分类的某个样本的数量小于1个,停止这次划分
            if shape(mat0)[0] < tolN or shape(mat1)[0] < tolN: continue;
            newS = errType(mat0) + errType(mat1)
            if newS < bestS:
                bestIndex = feat 
                bestValue = val 
                bestS = newS
    if (s - bestS) < tolS:
        return None,leafType(data)
    mat0,mat1 = splitData(data,bestIndex,bestValue)
    if shape(mat0)[0] < tolN or shape(mat1)[0] < tolN:
        return None,leafType(data)
    return bestIndex,bestValue

# 绘图
def plotData(data,i,j):
    data = mat(data)
    x = data[:,i]
    y = data[:,j]
    import matplotlib.pyplot as plt 
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.scatter(x.flatten().A[0],y.flatten().A[0])
    plt.show()

# 创建模型树
def createTree(data,leafType,errType,ops = (1,4)):
    feat,val = chooseBestSplit(data,leafType,errType,ops)
    if feat == None: return val
    retTree = {}
    retTree['spIdx'] = feat 
    retTree['spVal'] = val 

    lSet,rSet = splitData(data,feat,val)
    retTree['left'] = createTree(lSet,leafType,errType,ops)
    retTree['right'] = createTree(rSet,leafType,errType,ops)
    return retTree
    
data = loadData('exp2.txt')
plotData(data,0,1)
data = mat(data)
myTree = createTree(data,modelLeaf,modelErr,ops = (1,10))
print(myTree)