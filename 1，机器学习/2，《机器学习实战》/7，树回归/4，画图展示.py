from numpy import *
from tkinter import *
import numpy as np 
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure 

import compareModels as allTrees
# 加载数据
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t')) 
    data = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        data.append(tmp)
    return data

# 画树
def reDraw(tolS,tolN):
    reDraw.f.clf()
    reDraw.a = reDraw.f.add_subplot(111)
    if chkBtnVar.get():
        if tolN < 2: tolN = 2
        myTree = allTrees.createTree(reDraw.rawDat,allTrees.modelLeaf,\
                                    allTrees.modelErr,(tolS,tolN))
        yHat = allTrees.treeEval_all(myTree,reDraw.testDat,allTrees.modalTreeEval)
    else:
        myTree = allTrees.createTree(reDraw.rawDat,allTrees.regLeaf,\
                                    allTrees.regErr,(tolS,tolN))
        yHat = allTrees.treeEval_all(myTree,reDraw.testDat,allTrees.regTreeEval)
    reDraw.a.scatter(reDraw.rawDat[:,0].flatten().A[0],reDraw.rawDat[:,1].flatten().A[0],s = 5)
    reDraw.a.plot(reDraw.testDat,yHat,linewidth = 2.0)
    reDraw.canvas.show()

# 获取用户输入
def getInput():
    try: tolN = int(tolNentry.get())
    except: 
        tolN = 10 
        print("enter Integer for tolN")
        tolNentry.delete(0, END)
        tolNentry.insert(0,'10')
    try: tolS = float(tolSentry.get())
    except: 
        tolS = 1.0 
        print("enter Float for tolS")
        tolSentry.delete(0, END)
        tolSentry.insert(0,'1.0')
    return tolN,tolS

# 重画新树
def drawNewTree():
    tolN,tolS = getInput()
    reDraw(tolS,tolN)

root=Tk()

reDraw.f = Figure(figsize=(5,4), dpi=100) #create canvas
reDraw.canvas = FigureCanvasTkAgg(reDraw.f, master=root)
reDraw.canvas.show()
reDraw.canvas.get_tk_widget().grid(row=0, columnspan=3)

# ============== the same part =====================
Label(root, text="tolN").grid(row=1, column=0)
tolNentry = Entry(root)
tolNentry.grid(row=1, column=1)
tolNentry.insert(0,'10')

Label(root, text="tolS").grid(row=2, column=0)
tolSentry = Entry(root)
tolSentry.grid(row=2, column=1)
tolSentry.insert(0,'1.0')

Button(root, text="ReDraw", command=drawNewTree).grid(row=1, column=2, rowspan=3)
chkBtnVar = IntVar()
chkBtn = Checkbutton(root, text="Model Tree", variable = chkBtnVar)
chkBtn.grid(row=3, column=0, columnspan=2)

reDraw.rawDat = mat(loadData('sine.txt'))
reDraw.testDat = arange(min(reDraw.rawDat[:,0]),max(reDraw.rawDat[:,0]),0.01)
reDraw(1.0, 10)
root.mainloop()
