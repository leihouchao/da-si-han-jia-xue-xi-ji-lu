from numpy import * 
import numpy as np 

# 加载数据
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t'))
    data = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        data.append(tmp)
    return data  

# 分割数据
def splitData(data,feat,value):
    mat0 = data[nonzero(data[:,feat] > value)[0],:]
    mat1 = data[nonzero(data[:,feat] <= value)[0],:]
    return mat0,mat1 

# 回归树的树叶
def regLeaf(data):
    return mean(data[:,-1])

# 回归树的误差计算
def regErr(data):
    return var(data[:,-1] * shape(data)[0])

# 选择最优的feat
def chooseBestSplit(data,leafType,errType,ops=(1,4)):
    tolS = ops[0]; tolN = ops[1]
    if len(set(data[:,-1].T.tolist()[0])) == 1:
        return None,leafType(data)
    m,n = shape(data)
    S = errType(data)
    bestS = inf; bestIdx = 0; bestValue = 0 
    for feat in range(n-1):
        for val in set(data[:,feat].T.tolist()[0]):
            mat0,mat1 = splitData(data,feat,val)
            if shape(mat0)[0] < tolN or shape(mat1)[0] < tolN:
                continue 
            newS = errType(mat0) + errType(mat1)
            if newS < bestS:
                bestIdx = feat
                bestValue = val 
                bestS = newS 
    if (S - bestS) < tolS:
        return None,leafType(data)
    mat0,mat1 = splitData(data,bestIdx,bestValue)
    if shape(mat0)[0] < tolN or shape(mat1)[0] < tolN:
        return None,leafType(data)
    return bestIdx,bestValue

# 画图
def plotData(data,i,j):
    data = mat(data)
    x = data[:,i]
    y = data[:,j]
    import matplotlib.pyplot as plt 
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.scatter(x.flatten().A[0],y.flatten().A[0])
    plt.show()

# 创建树
def createTree(data,leafType,errType,ops=(1,4)):
    feat,val = chooseBestSplit(data,leafType,errType,ops)
    if feat == None: return val 
    retTree = {}
    retTree['spIdx'] = feat 
    retTree['spVal'] = val 
    
    lSet,rSet = splitData(data,feat,val)
    retTree['left'] = createTree(lSet,leafType,errType,ops)
    retTree['right'] = createTree(rSet,leafType,errType,ops)
    return retTree

# 剪枝
def isTree(obj):
    return (type(obj).__name__ == 'dict')
def getMean(tree):
    if isTree(tree['left']): tree['left'] = getMean(tree['left'])
    if isTree(tree['right']): tree['right'] = getMean(tree['right'])
    return (tree['left'] + tree['right'])/2
def prune(tree,testData):
    if shape(testData)[0] == 0: return getMean(tree)
    if (isTree(tree['right']) or isTree(tree['left'])):
        lSet,rSet = splitData(testData,tree['spIdx'],tree['spVal'])
    
    if(isTree(tree['left'])): tree['left'] = prune(tree['left'],lSet)
    if(isTree(tree['right'])): tree['right'] = prune(tree['right'],rSet)
    if not isTree(tree['left']) and not isTree(tree['right']):
        lSet,rSet = splitData(testData,tree['spIdx'],tree['spVal'])
        errNoMerge = sum(power(lSet[:,-1] - tree['left'],2)) + \
                     sum(power(lSet[:,-1] - tree['right'], 2))
        treeMean = (tree['left'] + tree['right']) / 2 
        errMerge = sum(power(testData[:,-1] - treeMean,2)) 
        if errMerge < errNoMerge:
            print("剪掉....")
            return treeMean 
        else: return tree 
    else: return tree 
    
data = loadData('ex2.txt')
data = mat(data) 
myTree = createTree(data,regLeaf,regErr, ops = (0,2))

testData = loadData('ex2test.txt')
testData = mat(testData)
newTree = prune(myTree,testData)
print(myTree)
    


