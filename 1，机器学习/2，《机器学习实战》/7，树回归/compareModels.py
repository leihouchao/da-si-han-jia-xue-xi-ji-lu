from numpy import * 
import numpy as np 

# 加载数据
def loadData(filename):
    n = len(open(filename).readline().strip().split('\t'))
    data = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split('\t')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        data.append(tmp)
    return data  
# 分割数据
def splitData(data,feat,val):
    mat0 = data[nonzero(data[:,feat] > val)[0],:]
    mat1 = data[nonzero(data[:,feat] <= val)[0],:]
    return mat0,mat1 

#=================== 回归树 ===================================
def regLeaf(data):
    return mean(data[:,-1])    
def regErr(data):
    return var(data[:,-1]) * shape(data)[0]
def isTree(obj):
    return (type(obj).__name__ == 'dict')
#=================== 模型树 ===================================
def linearSolve(data):
    m,n = shape(data)
    x = mat(ones((m,n)));
    y = mat(ones((m,1)))
    x[:,1:n] = data[:,0:n-1]
    y = data[:,-1]
    xTx = x.T * x 
    if linalg.det(xTx) == 0:
        print("矩阵不可逆...")
        return 
    w = xTx.I * x.T * y 
    return w,x,y 
def modelLeaf(data):
    w,x,y = linearSolve(data)
    return w 
def modelErr(data):
    w,x,y = linearSolve(data)
    yhat = x * w 
    return sum(power(yhat - y,2))

#=================== 创建树 ===================================
def chooseBestSplit(data,leafType,errType,ops = (1,4)):
    tolS = ops[0]
    tolN = ops[1]
    if len(set(data[:,-1].T.tolist()[0])) == 1:
        return None,leafType(data)
    m,n = shape(data)
    S = errType(data)
    bestS = inf; bestIdx = 0; bestValue = 0
    for feat in range(n-1):
        for val in set(data[:,feat].T.tolist()[0]):
            mat0,mat1 = splitData(data,feat,val)
            if shape(mat0)[0] < tolN or shape(mat1)[0] < tolN: continue 
            newS = errType(mat0) + errType(mat1)
            if newS < bestS:
                bestIdx = feat 
                bestValue = val 
                bestS = newS 
    if (S - bestS) < tolS:
        return None,leafType(data)
    mat0,mat1 = splitData(data,bestIdx,bestValue)
    if (shape(mat0)[0] < tolN or (shape(mat1)[0] < tolN)):
        return None,leafType(data)
    return bestIdx,bestValue 

# 绘图
def plotData(data,i,j):
    data = mat(data)
    x = data[:,i]
    y = data[:,j]
    import matplotlib.pyplot as plt 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x.flatten().A[0],y.flatten().A[0])
    plt.show()

# 创建树
def createTree(data, leafType ,errType  , ops=(1,4)):
    feat,val = chooseBestSplit(data,leafType,errType,ops)
    if feat == None: return val
    retTree = {}
    retTree['spIdx'] = feat
    retTree['spVal'] = val

    lSet,rSet = splitData(data,feat,val)
    retTree['left'] = createTree(lSet,leafType,errType,ops)
    retTree['right'] = createTree(rSet,leafType,errType,ops)
    return retTree

#===================== 评估 =====================================
def regTreeEval(modal,inDat):
    return float(modal)
def modalTreeEval(modal,inDat):
    n = shape(inDat)[1]
    x = mat(ones((1,n+1)))
    x[:,1:n+1] = inDat 
    return float(x*modal)
# 模型评价抽象
def treeEval_one(tree,inDat,modelEval):
    if not isTree(tree): return modelEval(tree,inDat)
    if inDat[tree['spIdx']] > tree['spVal']:
        if isTree(tree['left']):
            return treeEval_one(tree['left'],inDat,modelEval)
        else:
            return modelEval(tree['left'],inDat)
    else:
        if isTree(tree['right']):
            return treeEval_one(tree['right'],inDat,modelEval)
        else:
            return modelEval(tree['right'],inDat)
def treeEval_all(tree,testData,modelEval):
    m = len(testData)
    yhat = mat(zeros((m,1)))
    for i in range(m):
        yhat[i,0] = treeEval_one(tree,mat(testData[i]),modelEval)
    return yhat 

trainMat = mat(loadData('data_train.txt'))
testMat = mat(loadData('data_test.txt'))

myRegTree = createTree(trainMat,regLeaf,regErr,(1,20))
yhat = treeEval_all(myRegTree,testMat[:,0],regTreeEval)
print("回归树的准确率: ",corrcoef(yhat,testMat[:,1],rowvar = 0)[0,1])

myModelTree = createTree(trainMat,modelLeaf,modelErr,(1,20))
yhat = treeEval_all(myModelTree,testMat[:,0],modalTreeEval)
print("模型树的准确率是: ",corrcoef(yhat,testMat[:,1],rowvar = 0)[0,1])

w,x,y = linearSolve(trainMat)
for i in range(shape(testMat)[0]):
    yhat[i,0] = testMat[i,0] * w[1,0] + w[0,0]
print("线性模型: ",corrcoef(yhat,testMat[:,1],rowvar = 0)[0,1])
