from numpy import * 
import numpy as np 

# 由文档生成词汇表
def createVocablist(dataSet):
    vocabSet = set([])
    for doc in dataSet:
        vocabSet = vocabSet | set(doc)
    return list(vocabSet)

# 由word生成vector向量 -- 词集模型
def word2vec(vocabList,word):
    ret = [0] * len(vocabList)
    for w  in word:
        if w in vocabList:
            ret[vocabList.index(w)] = 1
    return ret 

# word生成vector向量 -- 文档词袋模型
def bagOfword2Vec(vocabList,word):
    ret = [0] * len(vocabList)
    for w in word:
        if w in vocabList:
            ret[vocabList.index(w)] += 1
    return ret 

# 朴素贝叶斯训练
def trainNB(trainMat,labels):
    numDocs = len(trainMat)
    numWords = len(trainMat[0])
    p1 = sum(labels) / float(numDocs)
    p0w = ones(numWords)
    p1w = ones(numWords)
    p0Count = 2
    p1Count = 2 

    for i in range(numDocs):
        if labels[i] == 1:
            p1w += trainMat[i]
            p1Count += sum(trainMat[i])
        else:
            p0w += trainMat[i]
            p0Count += sum(trainMat[i])
    p1vec = log(p1w/p1Count)
    p0vec = log(p0w/p0Count)
    return p0vec,p1vec,p1

# 分类器
def classify(vec,p0vec,p1vec,p1):
    p1 = sum(vec*p1vec) + log(p1)
    p0 = sum(vec*p0vec) + log(1-p1)
    if p1 > p0: return 1
    else: return 0

##########################################################################
# 句子转词列表
def textParse(str):
    import re 
    lists = re.split('\\W*',str)
    return [tok.lower() for tok in lists if len(tok)>0]

# 计算高频词
def clacMostFreq(vocabList,fullText):
    import operator 
    freqDict = {}
    for token in vocabList:
        freqDict[token] = fullText.count(token)
    sortedFreq = sorted(freqDict.items(),key = operator.itemgetter(1),reverse=True)
    return sortedFreq[:30]

# 整体测试函数
def Test_whole(feed1,feed0):
    import feedparser
    docList = []; classList = []; fullText = []
    minLen = min(len(feed1['entries']),len(feed0['entries']))
    for i in range(minLen):
        # 正例
        wordList = textParse(feed1['entries'][i]['summary'])
        docList.append(wordList)
        fullText.extend(wordList)
        classList.append(1)
        # 反例
        wordList = textParse(feed0['entries'][i]['summary'])
        docList.append(wordList)
        fullText.extend(wordList)
        classList.append(0)
    vocabList = createVocablist(docList)
    # 移除高频词汇
    top30Words = clacMostFreq(vocabList,fullText)
    for pair in top30Words:
        if pair[0] in vocabList:
            vocabList.remove(pair[0])
        
    # 数据集划分
    trainSet = list(range(2 * minLen));
    testSet = []
    for i in range(20):
        randIdx = int(random.uniform(0,len(trainSet)-1))
        testSet.append(trainSet[randIdx])
        del(trainSet[randIdx])
    trainMat = []; trainClass = []
    for docIndex in trainSet:
        trainMat.append(bagOfword2Vec(vocabList,docList[docIndex]))
        trainClass.append(classList[docIndex])
    
    # 训练,计算参数
    p0V,p1V,p1 = trainNB(array(trainMat),array(trainClass))
    errorCount = 0
    # 计算错误率
    for docIndex in testSet:
        wordVec = bagOfword2Vec(vocabList,docList[docIndex])
        if classify(array(wordVec),p0V,p1V,p1) != classList[docIndex]:
            errorCount += 1 
    
    print("错误率是: ",float(errorCount) /len(testSet))
    return vocabList,p0V,p1V 

# 计算最具表征性的词汇
def getTopWords(Pos,Neg):
    import operator
    vocaList,p0V,p1V = Test_whole(Pos,Neg)
    topPos = []
    topNeg = []
    print(p0V)
    for i in range(len(p0V)):
        if p0V[i] > -6: topNeg.append((vocaList[i],p0V[i]))
        if p1V[i] > -6: topPos.append((vocaList[i],p1V[i]))
    sortPos = sorted(topPos,key = lambda pair:pair[1], reverse=True)
    print("-----------------------------------------------")
    for item in sortPos:
        print(item[0])
    
    sortNeg = sorted(topNeg,key = lambda pair:pair[1], reverse=True)
    print("-----------------------------------------------")
    for item in sortNeg:
        print(item[0])
    
    

import feedparser 
PosData = feedparser.parse('http://newyork.craigslist.org/stp/index.rss')
NegData = feedparser.parse('http://sfbay.craigslist.org/stp/index.rss')
for i in range(10):
    Test_whole(PosData,NegData)
getTopWords(PosData,NegData)