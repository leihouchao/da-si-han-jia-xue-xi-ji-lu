from numpy import * 
import numpy as np 

# 创建文档
def loadData():
    x = [
        ['my', 'dog', 'has', 'flea', 'problems', 'help', 'please'],
        ['maybe', 'not', 'take', 'him', 'to', 'dog', 'park', 'stupid'],
        ['my', 'dalmation', 'is', 'so', 'cute', 'I', 'love', 'him'],
        ['stop', 'posting', 'stupid', 'worthless', 'garbage'],
        ['mr', 'licks', 'ate', 'my', 'steak', 'how', 'to', 'stop', 'him'],
        ['quit', 'buying', 'worthless', 'dog', 'food', 'stupid']
    ]
    y = [0,1,0,1,0,1]
    return x,y 
# 由文档生成词汇表
def createVocabList(data):
    vocabSet = set([])
    for doc in data:
        vocabSet = vocabSet | set(doc)
    return list(vocabSet)

# word -> vector 词集模型
def word2Vec(vocablist,word):
    ret = [0] * len(vocablist)
    for w in word:
        if w in vocablist:
            ret[vocablist.index(w)] = 1
    return ret 
# word -> vector 文档词袋模型
def bagOfword2Vec(vocablist,word):
    ret = [0] * len(vocablist)
    for w in word:
        if w in vocablist:
            ret[vocablist.index(w)] += 1
    return ret 

# 朴素贝叶斯分类器训练函数
def trainNB(trainMat,labels):
    numDocs = len(trainMat)
    numWords = len(trainMat[0])

    p1 = sum(labels) / float(numDocs)
    p0w = ones(numWords)
    p1w = ones(numWords)
    p0Count = 2
    p1Count = 2

    for i in range(numDocs):
        if labels[i] == 1:
            p1w += trainMat[i]
            p1Count += sum(trainMat[i])
        else:
            p0w += trainMat[i]
            p0Count += sum(trainMat[i])
    p1Vec = log(p1w / p1Count)
    p0Vec = log(p0w / p0Count)
    return p0Vec,p1Vec,p1

# 朴素贝叶斯分类器
def classifier(vec,p0vec,p1vec,p1):
    p1 = sum(vec * p1vec) + log(p1)
    p0 = sum(vec * p0vec) + log(1 - p1)
    if p1>p0: return 1
    else: return 0

# 测试函数
def NBClassifier():
    # 创建词汇表
    listPost,labels = loadData()
    myVocablist = createVocabList(listPost)
    # 创建
    trainMat = []
    for pos in listPost:
        trainMat.append(word2Vec(myVocablist,pos))
    p0v,p1v,p1 = trainNB(array(trainMat),array(labels))

    test0 = ['cute','my','dalmation']
    thisDoc = array(word2Vec(myVocablist,test0))
    print(test0, " 被分类为: ",classifier(thisDoc,p0v,p1v,p1))

    test0 =  ['stupid','garbage']
    thisDoc = array(word2Vec(myVocablist,test0))
    print(test0, " 被分类为: ",classifier(thisDoc,p0v,p1v,p1))

NBClassifier()