from numpy import * 
import numpy as np 

# 由文档生成词汇表
def createVocabList(data):
    vocabSet = set([])
    for doc in data:
        vocabSet = vocabSet | set(doc)
    return list(vocabSet)

# word -> vector 词集模型
def word2Vec(vocablist,word):
    ret = [0] * len(vocablist)
    for w in word:
        if w in vocablist:
            ret[vocablist.index(w)] = 1
    return ret 
# word -> vector 文档词袋模型
def bagOfword2Vec(vocablist,word):
    ret = [0] * len(vocablist)
    for w in word:
        if w in vocablist:
            ret[vocablist.index(w)] += 1
    return ret 

# 朴素贝叶斯分类器训练函数
def trainNB(trainMat,labels):
    numDocs = len(trainMat)
    numWords = len(trainMat[0])

    p1 = sum(labels) / float(numDocs)
    p0w = ones(numWords)
    p1w = ones(numWords)
    p0Count = 2
    p1Count = 2

    for i in range(numDocs):
        if labels[i] == 1:
            p1w += trainMat[i]
            p1Count += sum(trainMat[i])
        else:
            p0w += trainMat[i]
            p0Count += sum(trainMat[i])
    p1Vec = log(p1w / p1Count)
    p0Vec = log(p0w / p0Count)
    return p0Vec,p1Vec,p1

# 朴素贝叶斯分类器
def classifier(vec,p0vec,p1vec,p1):
    p1 = sum(vec * p1vec) + log(p1)
    p0 = sum(vec * p0vec) + log(1 - p1)
    if p1>p0: return 1
    else: return 0

# 测试函数
def NBClassifier():
    # 创建词汇表
    listPost,labels = loadData()
    myVocablist = createVocabList(listPost)
    # 创建
    trainMat = []
    for pos in listPost:
        trainMat.append(word2Vec(myVocablist,pos))
    p0v,p1v,p1 = trainNB(array(trainMat),array(labels))

    test0 = ['cute','my','dalmation']
    thisDoc = array(word2Vec(myVocablist,test0))
    print(test0, " 被分类为: ",classifier(thisDoc,p0v,p1v,p1))

    test0 =  ['stupid','garbage']
    thisDoc = array(word2Vec(myVocablist,test0))
    print(test0, " 被分类为: ",classifier(thisDoc,p0v,p1v,p1))

# 将句子切分成词列表
def textParse(str):
    import re 
    lists = re.split('\\W*',str)
    return [tok.lower() for tok in lists if len(tok)>0]

##################  使用朴素贝叶斯进行分类  ###################################################
# 垃圾邮件分类
def spamTest():
    docList = []
    labelList = []
    fullText = []
    for i in range(1,26):
        # 垃圾邮件统计
        wordList = textParse(open('spam/%d.txt' % i).read())
        docList.append(wordList)
        fullText.extend(wordList)
        labelList.append(1)
        # 非垃圾邮件统计
        wordList = textParse(open('ham/%d.txt' % i ).read())
        docList.append(wordList)
        fullText.extend(wordList)
        labelList.append(0)
    vocablist = createVocabList(docList)

    # 数据集划分 4:1
    trainSet = list(range(50))
    testSet = []
    for i in range(10):
        randIdx = int(random.uniform(0,len(trainSet)))
        testSet.append(trainSet[randIdx])
        del(trainSet[randIdx])
    
    # 将 训练数据集转化成向量集
    trainMat = []; trainClass = []
    for docIdx in trainSet:
        trainMat.append(word2Vec(vocablist,docList[docIdx]))
        trainClass.append(labelList[docIdx])
    # 计算p1,p0v,p1v
    p0v,p1v,p1 = trainNB(array(trainMat),array(trainClass))
    errorCount = 0 

    # 测试test集,计算正确率
    for docIdx in testSet:
        wordVec = word2Vec(vocablist,docList[docIdx])
        if classifier(array(wordVec),p0v,p1v,p1) != labelList[docIdx]:
            errorCount += 1
    print("\n\n朴素贝叶斯分类器的错误率是:", float(errorCount) / len(testSet))

spamTest()