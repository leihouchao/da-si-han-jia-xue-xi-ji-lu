from numpy import *
import numpy.linalg as la 

# 打印图像
def printMat(inMat,thresh = 0.8):
    for i in range(32):
        for k in range(32): 
            if float(inMat[i,k]) > thresh:
                print('1',end=' ')
            else: print('0',end=' ')
        print(' ')

# 图像压缩
def imgCompress(numSV = 3, thresh = 0.8):
    myL = []
    for line in open('0_5.txt').readlines():
        newRow = []
        for i in range(32):
            newRow.append(int(line[i]))
        myL.append(newRow)
    myMat = mat(myL)
    print("============ 原图像 =================")
    printMat(myMat,thresh)
    # 压缩
    U,sigma,VT = la.svd(myMat)
    sigRecon = mat(zeros((numSV,numSV)))
    for k in range(numSV):
        sigRecon[k,k] = sigma[k]
    reconMat = U[:,:numSV] * sigRecon * VT[:numSV,:]
    print('============= 压缩之后 =================')
    printMat(reconMat)

imgCompress(2)