from numpy import * 
import numpy as np 
from numpy import linalg as la 

# 生成数据
def makeData():
    return [[1,1,1,0,0],
            [2,2,2,0,0],
            [1,1,1,0,0],
            [5,5,5,0,0],
            [1,1,0,2,2],
            [0,0,0,3,3],
            [0,0,0,1,1]
        ]

# 主函数
U,sigma,VT = linalg.svd([[1,1],[7,7]])
print(U,sigma,VT)
print('============================================')

data = makeData()
U,sigma,VT = linalg.svd(data)
print(sigma)
print('============================================')

sig = [[sigma[0],0,0],[0,sigma[1],0],[0,0,sigma[2]]]
U = mat(U)
VT = mat(VT)
sig = mat(sig)
reData = U[:,:3] * sig * VT[:3,:]

reData = np.array(reData)
m,n = shape(reData)
for i in range(m):
    for j in range(n):
        if reData[i][j] < 0.01:
            reData[i][j] = 0
print(reData)