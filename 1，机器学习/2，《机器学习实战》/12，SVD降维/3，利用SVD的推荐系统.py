from numpy import *
import numpy.linalg as la

# 加载数据
def loadData():
    return [[0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 5],
           [0, 0, 0, 3, 0, 4, 0, 0, 0, 0, 3],
           [0, 0, 0, 0, 4, 0, 0, 1, 0, 4, 0],
           [3, 3, 4, 0, 0, 0, 0, 2, 2, 0, 0],
           [5, 4, 5, 0, 0, 0, 0, 5, 5, 0, 0],
           [0, 0, 0, 0, 5, 0, 1, 0, 0, 5, 0],
           [4, 3, 4, 0, 0, 0, 0, 5, 5, 0, 1],
           [0, 0, 0, 4, 0, 4, 0, 0, 0, 0, 4],
           [0, 0, 0, 2, 0, 2, 5, 0, 0, 1, 2],
           [0, 0, 0, 0, 5, 0, 0, 0, 0, 4, 0],
           [1, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0]]
    
# 计算相似度
def eSim(a,b):
    return 1.0/(1.0+la.norm(a-b))
def pSim(a,b):
    if len(a)<3: return 1.0
    return 0.5 + 0.5 * corrcoef(a,b,rowvar = 0)[0][1]
def cSim(a,b):
    num = float(a.T * b)
    denom = la.norm(a) * la.norm(b)
    return 0.5 + 0.5 * (num/denom)

# 基于svd的评分估计: 计算用户user给未知商品item的评分
def svdEst(data,user,simMean,item):
    n = shape(data)[0]
    simTotal = 0
    ratSimTotal = 0 

    U,sigma,VT = linalg.svd(data)
    sig = mat(eye(4) * sigma[:4])
    formedItems = data.T * U[:,:4] * sig.T

    for j in range(n):
        userRating = data[user,j]
        if userRating == 0 or j == item : continue
        similarity = simMean(formedItems[item,:].T, formedItems[j,:].T)
        simTotal += similarity 
        ratSimTotal += similarity * userRating
    if simTotal == 0: return 0 
    else: return ratSimTotal / simTotal
    
# 推荐商品
def recommend(data,user,estMethod,N = 3,simMean = cSim):
    unratedItems = nonzero(data[user,:].A == 0)[1]
    if len(unratedItems) == 0: return "你已经评价了所有商品"
    itemScores = [] 

    for item in unratedItems:
        estimateScore = estMethod(data,user,simMean,item)
        itemScores.append((item,estimateScore))
    return sorted(itemScores,key = lambda j: j[1],reverse = True)[:N]

data = mat(loadData())
rec = recommend(data,1,estMethod = svdEst)
print(rec)
rec = recommend(data,1,simMean = pSim,estMethod = svdEst)
print(rec)
