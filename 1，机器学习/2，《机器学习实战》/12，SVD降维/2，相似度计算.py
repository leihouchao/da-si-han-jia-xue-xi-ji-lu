from numpy import * 
import numpy as np 

# 生成数据
def makeData():
    return [[1,1,1,0,0],
            [2,2,2,0,0],
            [1,1,1,0,0],
            [5,5,5,0,0],
            [1,1,0,2,2],
            [0,0,0,3,3],
            [0,0,0,1,1]
        ]

# 计算相似度
def eSim(a,b):
    return 1.0 / (1.0 + linalg.norm(a-b))
def pSim(a,b):
    if len(a)<3: return 1.0
    return 0.5 + 0.5*corrcoef(a,b,rowvar = 0)[0][1]
def cSim(a,b):
    num = float(a.T * b)
    denom = linalg.norm(a) * linalg.norm(b)
    return 0.5 + 0.5 * (num/denom)

data = makeData()
data = mat(data)
print(eSim(data[:,0],data[:,4]))
print(eSim(data[:,0],data[:,0]))
print(cSim(data[:,0],data[:,4]))
print(cSim(data[:,0],data[:,0]))
print(pSim(data[:,0],data[:,4]))
print(pSim(data[:,0],data[:,0]))