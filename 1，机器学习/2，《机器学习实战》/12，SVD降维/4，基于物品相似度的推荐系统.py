from numpy import *
import numpy as np 
from numpy import linalg as la 

# 生成数据
def makeData():
    return [[4,4,0,2,2],
            [4,0,0,3,3],
            [4,0,0,1,1],
            [1,1,1,2,0],
            [2,2,2,0,0],
            [1,1,1,0,0],
            [5,5,5,0,0]
            ]

# 计算相似度
def eSim(a,b):
    return 1.0/(1.0+la.norm(a-b))
def pSim(a,b):
    if len(a)<3: return 1.0
    return 0.5 + 0.5 * corrcoef(a,b,rowvar = 0)[0][1]
def cSim(a,b):
    num = float(a.T * b)
    denom = la.norm(a) * la.norm(b)
    return 0.5 + 0.5 * (num/denom)

# 计算物品之间的相似度
def standEst(data,user,simMean,item):
    n = shape(data)[1]
    simTotal = 0; ratSimTotal = 0
    for j in range(n):
        userRating = data[user,j]
        if userRating == 0: continue 
        
        overlap = nonzero(logical_and(data[:,item].A > 0,data[:,j].A > 0))[0]
        if len(overlap) == 0: similarity = 0
        else: similarity = simMean(data[overlap,item],data[overlap,j])
        
        simTotal += similarity 
        ratSimTotal += similarity * userRating
    if simTotal == 0: return 0 
    else: return ratSimTotal / simTotal 
    
# 推荐商品
def recommend(data,user,N = 3,simMean = cSim,estMethod = standEst):
    unratedItems = nonzero(data[user,:].A == 0)[1]
    if len(unratedItems) == 0: return "你已经评价了所有商品"
    itemScore = []
    for item in unratedItems:
        estimateScore = estMethod(data,user,simMean,item)
        itemScore.append((item,estimateScore))
    return sorted(itemScore,key = lambda j : j[1],reverse = True)[:N]

data = mat(makeData())
rec = recommend(data,2)
print(rec)
rec = recommend(data,2,simMean = eSim)
print(rec)
rec = recommend(data,2,simMean = pSim)
print(rec)


