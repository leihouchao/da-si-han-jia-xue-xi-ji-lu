clear; close all; clc 

%===================== part 1 ===================================
% 加载数据
load('ex5data1.mat');
x = X;
m = size(x,1);
plot(x,y,'rx','MarkerSize',10,'LineWidth',1.5);

% 正则化回归: 计算Cost与梯度
function [J,grad] = computeCost(x,y,theta,lamda)
    m = length(y);
    J = 0;
    grad = zeros(size(theta));
    J = 1/(2*m) * sum((x * theta - y).^2 ) + lamda/(2*m)*(theta' * theta - theta(1) * theta(1)); %'

    grad = 1/m*(x' * (x * theta - y)) + lamda / m * theta; %'
    grad(1) = grad(1) - lamda/m*theta(1);
end

close all;
theta = [1;1];
[J,grad] = computeCost([ones(m,1) x],y,theta,1);
fprintf('cost: %f (303.993)\n',J)
fprintf("grad:%f\n",grad);
fprintf("1-15.30\n598.250\n");

%================= part 2 ===================================
% 使用fmincg训练线性模型
function [theta] = trainLinearReg(x,y,lamda)
    init_theta = zeros(size(x,2),1);
    costFunc = @(t) computeCost(x,y,t,lamda);
    option = optimset('MaxIter',200,'GradObj','on');
    theta = fmincg(costFunc,init_theta,option);
end
lamda = 0 ;
theta = trainLinearReg([ones(m,1) x],y,lamda);
plot(x,y,'rx','MarkerSize',10,'LineWidth',2);
hold on;
plot(x,[ones(m,1) x] * theta,'--','LineWidth',2);
hold off;

%================ part 3 =============================
% 绘制学习曲线
function [train_error,val_error] = learningCure(x,y,xval,yval,lamda)
    m = size(x,1);
    mval = size(xval,1);
    train_error = zeros(m,1);
    val_error = zeros(m,1);
    for i = 1:m
        x_tmp = x(1:i,:);
        y_tmp = y(1:i,:);
        theta = trainLinearReg(x_tmp,y_tmp,lamda);
        train_error(i) = 1/(2*i)*sum((x_tmp * theta - y_tmp).^2);
        val_error(i) = 1/(2*mval) * sum((xval * theta - yval).^2);
    end
end

lamda = 0;
[train_error,val_error] = learningCure([ones(m,1) x],y,[ones(size(Xval,1),1) Xval],yval,lamda);
plot(1:m,train_error,1:m,val_error);
axis([0 13 0 150]);

%================== part 4 ======================
% 数据归一化
function [x_norm,mu,sigma] = featureNorm(x)
    x_norm = zeros(size(x));
    [m,n] = size(x);
    mu = zeros(1,n);
    sigma = zeros(1,n);

    mu = mean(x);
    sigma = std(x);
    x_norm = (x - mu)./sigma;
end
% 将数据映射到高维空间
function [x_ploy] = mapFeature(x,p)
    x_ploy = zeros(numel(x),p);
    for i = 1:p
        x_ploy(:,i) = x.^i ;
    end
end
p = 8;
x_ploy = mapFeature(x,p);
[x_ploy,mu,sigma] = featureNorm(x_ploy);
x_ploy = [ones(m,1) x_ploy];

x_ploy_test = mapFeature(Xval,p);
x_ploy_test = (x_ploy_test - mu) ./ sigma;
x_ploy_test = [ones(size(x_ploy_test,1),1) x_ploy_test];

X_ploy_val = mapFeature(Xval,p);
X_ploy_val = (X_ploy_val - mu) ./ sigma;
X_ploy_val = [ones(size(X_ploy_val,1),1) X_ploy_val];

% 绘制多项式回归的拟合曲线
lamda = 0;
theta = trainLinearReg(x_ploy,y,lamda);
close all;
figure(1);
plot(x,y,'rx','MarkerSize',10,'LineWidth',1.5);
% 画图函数
function plotFit(min_x,max_x,mu,sigma,theta,p)
    hold on;
    x = (min_x - 15:0.05:max_x + 25)'; %'
    x_ploy = mapFeature(x,p);
    x_ploy = (x_ploy -mu) ./ sigma;
    x_ploy = [ones(size(x,1),1) x_ploy];
    plot(x,x_ploy * theta,'-','LineWidth',2);
    hold off;  
end

plotFit(min(x),max(x),mu,sigma,theta,p);

%===================== part 5 ======================
% 绘制学习曲线
figure(2);
[train_error,val_error] = learningCure(x_ploy,y,X_ploy_val,yval,lamda);
plot(1:m,train_error,1:m,val_error);
axis([0 13 0 150]);


%================ part 6 =====================
% 交叉验证选择参数 lamda 
function [lamda_vec,train_error,val_error] = validation(x,y,xval,yval);
    lamda_vec = [0 0.001 0.003 0.01 0.03 0.1 0.3 1 3 10]'; %'
    train_error = zeros(length(lamda_vec),1);
    val_error = zeros(length(lamda_vec),1);
    m = size(x,1);
    mval = size(xval,1);

    for i = 1:length(lamda_vec)
        lamda = lamda_vec(i);
        theta = trainLinearReg(x,y,lamda);
        train_error(i) = 1/(2*m)*sum((x * theta - y).^2);
        val_error(i) = 1/(2*mval)*sum((xval * theta - yval).^2);
    end
end

[lamda_vec,train_error,val_error] = validation(x_ploy,y,X_ploy_val,yval);
close all;
plot(lamda_vec,train_error,lamda_vec,val_error);