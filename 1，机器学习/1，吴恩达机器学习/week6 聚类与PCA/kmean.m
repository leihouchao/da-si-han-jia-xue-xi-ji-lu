clear; close all; clc 

% 加载数据
load('keamData.mat');
k = 3;
init_centers = [3 3;6 2;8 5];

%===================== part 1 ===========================
% 计算每个样本所属的中心点类型
function idx = findClosestCenter(x,centers)
    k = size(centers,1);
    m = size(x,1);
    idx = zeros(m,1);
    for i = 1:m
        diff = centers - x(i,:);
        sumDiff = sum(diff.^2,2); 
        [~,minIdx] = min(sumDiff);
        idx(i) = minIdx;
    end
end

idx = findClosestCenter(X,init_centers);
fprintf('%d \n',idx(1:3));
fprintf('(1,3,2)\n')

%======================= part 2 ================================
% 重新计算中心点
function centers = computeCenter(x,idx,k)
    [m,n] = size(x);
    centers = zeros(k,n);
    for i = 1:k
        tmpIdx = find(idx == i);
        tmpX = x(tmpIdx,:);
        centers(i,:) = mean(tmpX);
    end
end
centers = computeCenter(X,idx,k);
fprintf("[ %f, %f]\n",centers(1,:));
fprintf('[ 2.428301, 3.157924 ]\n\n');

fprintf("[ %f, %f]\n",centers(2,:));
fprintf('[ 5.813503, 2.633656 ]\n\n');

fprintf("[ %f, %f]\n",centers(3,:));
fprintf('[ 7.119387, 3.616684 ]\n\n');

%========================== part 3 ==============================
% 运行k-mean算法
function plotDataPoint(x,idx,k)
    palette = hsv(k+1);
    colors = palette(idx,:);
    scatter(x(:,1),x(:,2),15,colors);
end
function drawLine(p1,p2)
    plot([p1(1) p2(1)],[p1(2) p2(2)]);
end
function plotProgressKmeans(x,centers,previous,idx,k)
    plotDataPoint(x,idx,k);
    plot(centers(:,1),centers(:,2),'x','MarkerEdgeColor','g','MarkerSize', 10, 'LineWidth', 3);
    for j = 1:size(centers,1)
        drawLine(centers(j,:),previous(j,:))
        pause(0.2)
    end
end
% k-mean算法
function [centers,idx] = kMean(x,init_centers,max_iter,ifPlot)
    [m,n] = size(x);
    k = size(init_centers,1);
    centers = init_centers;
    previous = centers;
    idx = zeros(m,1);

    if ifPlot
        figure; hold on;
    end 

    for i = 1:max_iter
        idx = findClosestCenter(x,centers);
        if ifPlot
            plotProgressKmeans(x,centers,previous,idx,k);
            previous = centers;
        end
        centers = computeCenter(x,idx,k);
    end

    if ifPlot
        hold off;
    end
end

load('keamData.mat')
k = 3; max_iter = 10;
init_centers = [3 3;6 2;8 5];
[centers,idx] = kMean(X,init_centers,max_iter,true);

%========================== part 4 ==============================
% 使用k-mean压缩图像
A = double(imread('bird_small.png'));
A = A/255 ;
img_size = size(A);
X = reshape(A,img_size(1) * img_size(2),3);
k = 16 ;
max_iter = 10;

% 随机初始化中心点(从数据集中随机选择k个点)
function centers = randomInitCenters(x,k)
    centers = zeros(k,size(x,2));
    randIdx = randperm(size(x,1));
    centers = x(randIdx(1:k),:);
end

init_centers = randomInitCenters(X,k);
[centers,idx] = kMean(X,init_centers,max_iter,false);

% 压缩图像
idx = findClosestCenter(X,centers);
x_recover = centers(idx,:);
x_recover = reshape(x_recover,img_size(1),img_size(2),3);

% 显示图像
subplot(1,2,1);
imagesc(A);
subplot(1,2,2);
imagesc(x_recover);