clear; close all;clc 

%================= part 1 ========================
% 加载数据,显示
load('pcaData.mat');
plot(X(:,1),X(:,2),'rx');
axis([0.5 6.5 2 8]);
axis square ;

%================= part 2 =========================
% 数据归一化
function [x_norm,mu,sigma] = featureNorm(x)
    [m,n] = size(x);
    x_norm = zeros(m,n);
    mu = mean(x);
    sigma = std(x);
    x_norm = (x - mu) ./ sigma;
end
[x_norm,mu,sigma] = featureNorm(X);

% pca算法
function [u,s] = Pca(x)
    [m,n] = size(x);
    u = zeros(n);
    s = zeros(n);

    sigma = x' * x / m ; %'
    [u,s,v] = svd(sigma);
end

% 画线
function drawLine(p1,p2)
    plot([p1(1) p2(1)],[p1(2) p2(2)]);
end

[u,s] = Pca(x_norm);
hold on;
drawLine(mu, mu + 1.5 * s(1,1) * u(:,1)', '-k', 'LineWidth', 2); %'
drawLine(mu, mu + 1.5 * s(2,2) * u(:,2)', '-k', 'LineWidth', 2); %'
hold off ;
fprintf(' U(:,1) = %f %f \n', u(1,1), u(2,1));  
fprintf('(you should expect to see -0.707107 -0.707107)\n\n');  

%================= part 3 =========================
% 降维
plot(x_norm(:,1),x_norm(:,2),'bo');
axis([-4 3 -4 3]);
axis square;
k = 1;

% 数据映射
function z = projectData(x,u,k)
    z = zeros(size(x,1),k);
    ureduce = u(:,1:k);
    z = x * ureduce;
end
z = projectData(x_norm,u,k);
fprintf('Projection of the first example: %f\n', z(1));  
fprintf('(this value should be about 1.481274)\n\n'); 

% 数据重构
function x_rec = recoverData(z,u,k)
    x_rec = zeros(size(z,1),size(u,1));
    ureduce = u(:,1:k);
    x_rec = z * ureduce'; %'
end
x_rec = recoverData(z,u,k);
fprintf('Approximation of the first example: %f %f\n', x_rec(1, 1), x_rec(1, 2));
fprintf('\n(this value should be about  -1.047419 -1.047419)\n\n');

hold on;
plot(x_rec(:,1),x_rec(:,2),'ro');
for i = 1:size(x_norm,1)
    drawLine(x_norm(i,:),x_rec(i,:),'--k','LineWidth',1);
end
hold off;

%============================= part 4 ======================
% 利用pca对face图像进行压缩
load('pcafaces.mat');
% 画图函数
function [h,display_array] = displayData(X)
    example_width = round(sqrt(size(X,2)));
    colormap(gray);
    [m,n] = size(X);
    example_height = n/example_width;

    display_row = floor(sqrt(m));
    display_col = ceil(m/display_row);

    pad = 1;
    display_array = -ones(pad + display_row * (example_height + pad), ...
                          pad + display_col * (example_width + pad));
    curr_ex = 1;
    for j = 1:display_row
        for i = 1:display_col
            if curr_ex > m
                break;
            end
            max_val = max(abs(X(curr_ex,:)));
            display_array(pad + (j - 1) * (example_height + pad) + (1:example_height), ...
                    pad + (i - 1) * (example_width + pad) + (1:example_width)) = ...
                    reshape(X(curr_ex, :), example_height, example_width) / max_val;
		    curr_ex = curr_ex + 1;
        end
        if curr_ex > m 
            break;
        end 
    end

    h = imagesc(display_array, [-1 1]);
    axis image off;
    drawnow;
end
displayData(X(1:100,:));
[x_norm,mu,sigma] = featureNorm(X);
[u,s] = Pca(x_norm);
displayData(u(:,1:36)'); %'

%=============================== part 5 ==================================
% 对face降维
k = 100;
z = projectData(x_norm,u,k);
% 对face重构
x_rec = recoverData(z,u,k);
subplot(1,2,1);
displayData(x_norm(1:100,:));
axis square;

subplot(1,2,2)
displayData(x_rec(1:100,:));
axis square;

%======================== part 6 ========================
% 对bird图片进行压缩
% 1,k-mean算法
% 计算每个样本所属的中心点类型
function idx = findClosestCenters(X,centers)
    K = size(centers,1);
    m = size(X,1);
    idx = zeros(m,1);
    for i = 1:m
        dif = centers - X(i,:);
        sumdif = sum(dif.^2,2); % 按行累加
        [~,minidx] = min(sumdif);
        idx(i) = minidx;
    end
end
% 重新计算中心点
function centers =computeCenter(X,idx,k)
    [m,n] = size(X);
    centers = zeros(k,n);
    for i = 1:k 
        tmpidx = find(idx == i); % 返回索引
        tmpX = X(tmpidx,:);
        centers(i,:) = mean(tmpX);
    end
end
% 随机初始化中心点(从数据集中随机选k个点)
function centers = randomInitCenters(X,k)
    centers = zeros(k,size(X,2));

    randix = randperm(size(X,1));
    centers = X(randix(1:k),:);
end
% kMean算法
function [centers,idx] = runKmeans(X,init_center,max_Iter)
    [m,n] = size(X);
    k = size(init_center,1);
    centers = init_center;
    idx = zeros(m,1);
    for i = 1:max_Iter
        idx = findClosestCenters(X,centers);
        centers = computeCenter(X,idx,k);
    end
end

% 1,使用k-mean聚类128*128 -> 16
close all;clear; clc
A = double(imread('bird_small.png'));
A = A/255;
img_size = size(A);
x = reshape(A,img_size(1) * img_size(2),3);
k = 16;
max_Iter = 10;

init_center = randomInitCenters(x,k);
[centers,idx] = runKmeans(x,init_center,max_Iter);

sel = floor(rand(1000,1) * size(x,1)) + 1;
palette = hsv(k);
colors = palette(idx(sel),:);

figure(1);
scatter3(x(sel,1),x(sel,2),x(sel,3),10,colors);

% 2,将数据pca降维到2维显示
[x_norm,mu,sigma] = featureNorm(x);
[u,s] = Pca(x_norm);
z = projectData(x_norm,u,2);
figure(2);

% 画图
function plotDataPoint(x,idx,k)
    palette = hsv(k + 1);
    colors = palette(idx,:);
    scatter(x(:,1),x(:,2),15,colors);
end
plotDataPoint(z(sel,:),idx(sel),k);