clear;
close all;
clc;

% 1,画图函数
function plotData(x,y)
    figure;
    plot(x,y,'rx','MarkerSize',10);
    xlabel("population");
    ylabel('profit');
end 

% 可视化数据
fprintf("plot data...\n");
data = load("./regData.txt");
X = data(:,1);
y = data(:,2);
m = length(y);
plotData(X,y)

% 2,计算cost函数值
function J = computeCost(x,y,theta)
    m = length(y);
    h = x * theta;
    errorValue = h - y ;
    % J = 1/(2*m) * sum(errorValue.^2);
    J = 1 / (2*m) * (errorValue' * errorValue);    % '
end

X = [ones(m,1),data(:,1)];
theta = zeros(2,1);
iter = 1500;
alpha = 0.01 ;
J = computeCost(X,y,theta);
fprintf("the J of [0;0] is %f (32.07)\n",J);
J = computeCost(X,y,[-1;2]);
fprintf("the J of [-1;2] is %f (54.24)\n",J);

% 3,梯度下降计算最优theta值
function [theta,Js] = gradientDescent(x,y,theta,alpha,Iters)
    m = length(y);
    Js = zeros(Iters,1);
    for iter = 1 : Iters
        tmp0 = theta(1) - alpha/m*sum(x*theta - y);
        tmp1 = theta(2) - alpha/m*sum((x*theta - y )' * x(:,2)); %'
        theta(1) = tmp0;
        theta(2) = tmp1;
        Js(iter) = computeCost(x,y,theta);
    end
end

[theta,Js] = gradientDescent(X,y,theta,alpha,iter);
fprintf("best iter is: \n");
fprintf("%f \n",theta);
fprintf("which should be (-3.6303, 1.1664)\n ");

% 画出最优直线
hold on;
plot(X(:,2), X*theta, '-')
hold off

figure;
% 画出J(theta)曲线
plot(Js,'r-')

% 画碗图和投影图
function drawBowl(X,y,theta)
    theta0 = linspace(-10,10,100);
    theta1 = linspace(-1,4,100);
    J_vals = zeros(length(theta0),length(theta1));

    for i = 1:length(theta0)
        for j = 1:length(theta1)
            t = [theta0(i); theta1(j)];
            J_vals(i,j) = computeCost(X,y,t);
            % fprintf("%f\n",t(1))
        end
    end

    J_vals = J_vals'; %'

    % 碗图
    figure;
    surf(theta0,theta1,J_vals)
    xlabel('\t theta0')
    ylabel('\t theta1')

    % 投影图
    figure;
    contour(theta0,theta1,J_vals,logspace(-2,3,20))
    xlabel('\t theta0')
    ylabel('\t theta1')

    % 绘点
    hold on 
    plot(theta(1),theta(2),'rx','MarkerSize',10,'LineWidth',2)
end

drawBowl(X,y,theta)