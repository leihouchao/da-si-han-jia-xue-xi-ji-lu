from numpy import *
import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

# 画图函数
def plotData(x,y):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    x = np.array(x)
    y = np.array(y)
    ax.scatter(x,y)
    plt.show()

# 加载数据
def loadData(filename):
    x = []; y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split(',')
        x.append(float(line[0]))
        y.append(float(line[1]))
    x = mat(x).T; y = mat(y).T
    return x,y 

# 计算cost函数值
def computeCost(x,y,theta):
    m = shape(x)[0]
    x = mat(x); theta = mat(theta); y = mat(y)
    h = x * theta 
    err = h - y 
    J = 1/(2*m)*(err.T * err)
    return J

# 辅助函数
def help_addones(x):
    m,n = shape(x)
    x = np.array(x)
    ret = []
    for i in range(m):
        tmp = []
        tmp = [1,x[i][0]]
        ret.append(tmp)
    return ret 

# 梯度下降计算最优theta值
def gradientDescent(x,y,theta,alpha,Iters):
    m = shape(x)[0]
    Js = zeros((Iters,1))
    x = mat(x); y = mat(y); theta = mat(theta)
    for iters in range(Iters):
        tmp0 = theta[0] - alpha/m*sum(x*theta - y)
        tmp1 = theta[1] - alpha/m*sum((x*theta - y).T * x[:,1])
        theta[0] = tmp0 
        theta[1] = tmp1 
        Js[iters] = computeCost(x,y,theta)
    return theta,Js

# 画出最优直线
def plotBestFitLine(x,y,theta):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    # 画点
    x = np.array(x)
    y = np.array(y)
    ax.scatter(x[:,1],y)
    # 画线
    ax.plot(x[:,1],x*theta,'r-')
    plt.show()

# 画出Js随着迭代的关系
def plotJs(Js):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.plot(Js,'g--')
    plt.show()

# 计算J_vals值
def computeJ_vals(x,y):
    theta0 = np.arange(-10,10,0.2)
    theta1 = np.arange(-1,4,0.05)
    J_vals = zeros((len(theta0),len(theta1)))
    # 计算碗图的值
    for i in range(len(theta0)):
        for j in range(len(theta1)):
            t = [[theta0[i]],[theta1[j]]]
            J_vals[i,j] = computeCost(x,y,t)
    return theta0,theta1,J_vals
# 画碗图
def drawBowl(x,y):
    theta0,theta1,J_vals = computeJ_vals(x,y);
    fig = plt.figure() 
    ax = Axes3D(fig)
    theta0,theta1 =  np.meshgrid(theta0,theta1)
    ax.plot_surface(theta0,theta1,J_vals, rstride=1, cstride=1, cmap='rainbow')
    plt.show()
# 画投影图
def drawContour(x,y,theta):
    theta0,theta1,J_vals = computeJ_vals(x,y);
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.contour(theta0,theta1,J_vals,logspace(-2,3,20))
    ax.plot(theta[0],theta[1],'rx')
    plt.show()


# 主函数
def main():
    x,y = loadData('regData.txt')
    plotData(x,y)
    print('===================================')
    x = help_addones(x)
    print(shape(x))
    print('===================================')
    theta = zeros((2,1))
    Iters = 1500
    alpha = 0.01 
    J = computeCost(x,y,theta)
    print('J = ',J,' (32.07)')
    J = computeCost(x,y,[[-1],[2]])
    print('J = ',J,' (54.24)')
    print('===================================')
    [theta,Js] = gradientDescent(x,y,theta,alpha,Iters)
    print("最优theta是: \n",theta,' (-3.6303,1.1664)')
    print('===================================')
    plotBestFitLine(x,y,theta);
    print('===================================')
    plotJs(Js)
    print('===================================')
    drawBowl(x,y)
    drawContour(x,y,theta)

main()