from numpy import *
import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

# 加载数据
def loadData(filename):
    x = []; y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split(',')
        x.append([float(line[0]),float(line[1])])
        y.append(float(line[2]))
    x = mat(x); y = mat(y).T
    return x,y
# 数据归一化
def normData(x):
    m,n = shape(x)
    mu = zeros((1,n))
    sigma = zeros((1,n))
    print(mu)
    mu = mean(x,axis = 0)
    sigma = std(x,axis = 0)
    x = (x - mu)/sigma 
    return x,mu,sigma 
# 辅助函数
def help_addones(x):
    m,n = shape(x)
    x = np.array(x)
    ret = []
    for i in range(m):
        tmp = []
        tmp = [1,x[i][0],x[i][1]]
        ret.append(tmp)
    return ret 
# 计算cost函数
def computeCost(x,y,theta):
    x = mat(x); y = mat(y); theta = mat(theta)
    m = shape(x)[0]
    h = x * theta 
    err = (h - y)
    J = 1/(2*m)*(err.T * err)
    return J 
# 梯度下降
def gradientDescent(x,y,theta,alpha,Iters):
    m,n = shape(x)
    Js = zeros((Iters,1))
    x = mat(x); y = mat(y); theat = mat(theta)
    for iters in range(Iters):
        theta = theta - alpha/m*(x.T * (x * theta - y))
        Js[iters] = computeCost(x,y,theta)
    return theta,Js
# 画出Js随着迭代的关系
def plotJs(Js):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.plot(Js,'g--')
    plt.show()
# 测试不同学习速率对收敛的效果
def testAlpha(x,y):
    alpha1 = 0.1; alpha2 = 0.05; alpha3 = 0.01;
    theta = zeros((3,1))
    [_,J1] = gradientDescent(x,y,theta,alpha1,400)
    theta = zeros((3,1))
    [_,J2] = gradientDescent(x,y,theta,alpha2,400)
    theta = zeros((3,1))
    [_,J3] = gradientDescent(x,y,theta,alpha3,400)
    
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.plot(J1,'r--')
    ax.plot(J2,'g--')
    ax.plot(J3,'b--')
    plt.show()
# 正规方程求解
def standEqn(x,y):
    theta = zeros((shape(x)[1],1))
    x = mat(x); y = mat(y)
    theta = linalg.pinv(x.T * x)*x.T * y 
    return theta


# 主函数
def main():
    x,y = loadData('multiRegData.txt')
    print(shape(x))
    print(shape(y))
    print('******************************************')
    x,mu,sigma = normData(x)
    print(mu)
    print(sigma)
    print('******************************************')
    x = help_addones(x)
    print(shape(x))
    print('******************************************')
    alpha = 0.01 
    Iters = 400 
    theta = zeros((3,1))
    [theta,Js] = gradientDescent(x,y,theta,alpha,Iters)
    print("最优theta是: ",theta)
    plotJs(Js)
    print('******************************************')
    testAlpha(x,y)
    print('******************************************')
    print("正规方程求解: ",standEqn(x,y))

main()  
