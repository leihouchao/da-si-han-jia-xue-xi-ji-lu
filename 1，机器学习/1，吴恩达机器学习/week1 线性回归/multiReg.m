clear; close all; clc 

% 1,加载数据
data = load("multiRegData.txt");
X = data(:,1:2);
y = data(:,3);
m = length(y);

% 2,归一化数据
function [X_norm,mu,sigma] = featNorm(X)
    X_norm = X ;
    mu = zeros(1,size(X,2));
    sigma = zeros(1,size(X,2));

    mu = mean(X);
    sigma =  std(X);
    X_norm = (X - mu)./sigma ;
end
[X mu sigma] = featNorm(X);
% 添加x0 = 1的列
X = [ones(m,1) X];

% 3,使用梯度下降法计算最优theta
function J = computeCost(x,y,theta)
    m = length(y);
    J = 0;
    h = x * theta ;
    errorNum = (h - y).^2;
    J = 1/(2*m) * sum(errorNum);
end

function [theta,Js] = gradientDescent(x,y,theta,alpha,Iters)
    m = length(y);
    Js = zeros(Iters,1);
    n = size(x,2);

    for iter = 1:Iters
        theta = theta - alpha / m * (x' * (x * theta - y)); % '
        Js(iter) = computeCost(x,y,theta);
    end

end

alpha = 0.01 ;
Iters = 400 ;
theta = zeros(3,1);
[theta,Js] = gradientDescent(X,y,theta,alpha,Iters);
fprintf("theta: %f\n ",theta);

% 4,画图J(theta)
figure;
plot(Js,'b-','LineWidth',2);
xlabel('iter num')
ylabel('J')

% 5,测试不同学习速率对收敛的效果
alpha1 = 0.1; alpha2 = 0.05; alpha3 = 0.01;
theat = zeros(3,1);
[_,J1] = gradientDescent(X,y,theat,alpha1,Iters);
theat = zeros(3,1);
[_,J2] = gradientDescent(X,y,theat,alpha2,Iters);
theat = zeros(3,1);
[_,J3] = gradientDescent(X,y,theat,alpha3,Iters);
close all;
plot(J1,'b-','LineWidth',2);
hold on;
plot(J2,'g-','LineWidth',2);
plot(J3,'r-','LineWidth',2);

% 正规方程求解
function [theta] = normEqn(x,y)
    theta = zeros(size(x,2),1);
    theta = pinv(x' * x) * x' * y;
end
data = csvread('multiRegData.txt');
x = data(:,1:2);
y = data(:,3);
m = length(y);
x = [ones(m,1) x];

[theta] = normEqn(x,y);
fprintf("best theta is: %f\n" ,theta);