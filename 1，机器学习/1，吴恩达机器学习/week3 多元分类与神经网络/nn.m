close all; clear; clc 

input_layer_size = 400 ;
hidden_layer_size = 25 ;
num_labels = 10 ;

% 加载数据
load('ex3data1.mat');
m = size(X,1);

% 画图函数
function [h,display_array] = displayData(X)
    example_width = round(sqrt(size(X,2)));
    colormap(gray);
    [m,n] = size(X);
    example_height = n/example_width;

    display_row = floor(sqrt(m));
    display_col = ceil(m/display_row);

    pad = 1;
    display_array = -ones(pad + display_row * (example_height + pad), ...
                          pad + display_col * (example_width + pad));
    curr_ex = 1;
    for j = 1:display_row
        for i = 1:display_col
            if curr_ex > m
                break;
            end
            max_val = max(abs(X(curr_ex,:)));
            display_array(pad + (j - 1) * (example_height + pad) + (1:example_height), ...
                    pad + (i - 1) * (example_width + pad) + (1:example_width)) = ...
                    reshape(X(curr_ex, :), example_height, example_width) / max_val;
		    curr_ex = curr_ex + 1;
        end
        if curr_ex > m 
            break;
        end 
    end

    h = imagesc(display_array, [-1 1]);
    axis image off;
    drawnow;
end

sel = randperm(size(X,1));
sel = sel(1:100);
displayData(X(sel,:));

% 加载权重
load('ex3weights.mat')
% sigmoid函数
function h = sigmoid(z)
    h = 1./(1+exp(-z));
end
% 利用已有w做预测
function p = predict(theta1,theta2,x)
    m = size(x,1);
    num_labels = size(theta2,1);
    p = zeros(m,1);

    a1 = [ones(m,1) x];
    z2 = a1 * theta1'; %' 
    a2 = sigmoid(z2);
    a2 = [ones(m,1) a2];
    z3 = a2 * theta2' ; %'
    a3 = sigmoid(z3);
    [~,p] = max(a3,[],2);
end

pred = predict(Theta1,Theta2,X);
fprintf('%d %d\n',pred(1:10),y(1:10))
fprintf('acc: %f\n',mean(double(pred == y))*100);

% 显示预测的例子
rp = randperm(m);
for i = 1:m 
    fprintf('\n show image\n')
    displayData(X(rp(i),:));
    pred = predict(Theta1,Theta2,X(rp(i),:));
    fprintf('predict: %d (digit %d)\n',pred,mod(pred,10));
    s = input('Paused - press enter to continue, q to exit:','s');
    if s == 'q'
        break
    end
end