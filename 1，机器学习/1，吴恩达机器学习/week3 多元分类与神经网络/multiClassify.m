clear; close all; clc 

input_layer_size = 400 ;
num_labels = 10 ;

% 加载数据
load('ex3data1.mat');
m = size(X,1);

% 随机选择100个样本,展示
rand_idx = randperm(m);
sel = X(rand_idx(1:100),:);

% 画图函数
function [h,display_array] = displayData(x)
    example_width = round(sqrt(size(x,2)));
    colormap(gray);
    [m,n] = size(x);
    example_height = n/example_width;

    display_row = floor(sqrt(m));
    display_col = ceil(m/display_row);

    pad = 1;
    display_array = -ones(pad + display_row * (example_height + pad), ...
                          pad + display_col * (example_width + pad)
                    );
    cur_ex = 1;
    for j = 1:display_row
        for i = 1:display_col
            if cur_ex > m 
                break
            end 
            max_val = max(abs(x(cur_ex,:)));
            display_array(pad + (j-1) * (example_height + pad) + (1:example_height), ...
                          pad + (i-1) * (example_width + pad) + (1:example_width)) ...
                =  reshape(x(cur_ex,:),example_height,example_width)/max_val;
            cur_ex = cur_ex + 1;
        end
        if cur_ex > m 
            break 
        end
    end

    h = imagesc(display_array,[-1 1]);
    axis image off;
    drawnow;
end 
[h,d] = displayData(sel);

% sigmoid函数
function h = sigmoid(z)
    h = 1./(1 + exp(-z));
end
% 计算cost与梯度
function [J,grad] = computeCost(x,y,theta,lamda)
    m = length(y);
    J = 1/m * sum(-y .* log(sigmoid(x * theta)) - (1-y) .* log(1-sigmoid(x * theta))) ...
        + lamda/(2*m)*(theta' * theta - theta(1) * theta(1)) ;  %'
    
    grad = zeros(size(theta));
    grad = 1/m * x' * (sigmoid(x * theta) - y) + lamda / m * theta ; %'
    grad(1) = grad(1) - lamda/m*theta(1);
    grad = grad(:);
end

theta_t = [-2; -1; 1; 2];
x_t = [ones(5,1) reshape(1:15,5,3)/10];

fprintf("%f ",x_t(1))

y_t = ([1;0;1;0;1] > 0.5);
lamda_t = 3;
[J,grad] = computeCost(x_t,y_t,theta_t, lamda_t);
fprintf('cost: %f (2.534819)\n',J)
fprintf('%f\n',grad);
fprintf("0.146561 -0.548558 0.724722 1.398003\n")

% 计算多元分类的theta值
function [all_theta] = oneVsAll(x,y,num,lamda)
    [m,n] = size(x);
    all_theta = zeros(num,n+1);
    x = [ones(m,1) x];

    for i = 1:num
        init_theta = zeros(n+1,1);
        options = optimset('GradObj', 'on', 'MaxIter', 50);
        [theta] = fmincg (@(t)(computeCost(x, (y == i), t,lamda)), ...
                        init_theta, options);
        all_theta(i,:) = theta ;
    end
end

lamda = 0.1;
[all_theta] = oneVsAll(X,y,num_labels,lamda);
% 做预测
function p = predictOneVsAll(all_theta,x)
    m = size(x,1);
    num = size(all_theta,1);
    p = zeros(m,1);
    x = [ones(m,1) x];
    [~,p] = max(x * all_theta',[],2) ;%'
end

pred = predictOneVsAll(all_theta,X);
fprintf('acc: %f\n',mean(double(pred == y))*100);
