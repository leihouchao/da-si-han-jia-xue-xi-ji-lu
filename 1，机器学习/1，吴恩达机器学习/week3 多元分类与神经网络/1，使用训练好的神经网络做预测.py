import scipy.io as sio  
import matplotlib.pyplot as plt  
import numpy as np  
from numpy import *
from PIL import Image
import random 

# 加载数据
def loadData(filename):
    data = sio.loadmat(filename)
    x = data['X']
    y = data['y']
    print(shape(x))
    print(shape(y))
    return x,y 
#  加载权重
def loadWight(filename):
    data = sio.loadmat(filename)
    Theta1 = data['Theta1']
    Theta2 = data['Theta2']
    print(shape(Theta1))
    print(shape(Theta2))
    return Theta1,Theta2
# 预处理一下数据
def prepareX(x):
    m,n = shape(x)
    for i in range(m):
        for j in range(n):
            x[i,j] = floor((x[i,j] + 0.2) * 180)
    return x
# 画图函数展示数据
def displayData(x):
    m,n = shape(x);
    h = int(sqrt(n))
    w = h;
    row = int(sqrt(m))
    col = row;

    pad = 1
    dis_array = zeros((pad + row * (h + pad),pad + col * (w + pad)));
    x = prepareX(x)
    k = 0
    m,n = shape(dis_array)
    for i in range(m):
        for j in range(n):
            dis_array[i,j] = 255
    for i in range(row): 
        for j in range(col):
            dis_array[i*h +(i+1)*pad:(i+1)*h +(i+1)*pad,j*w +(j+1)*pad:(j+1)*w +(j+1)*pad] = reshape(x[k],(h,w))
            k += 1

    new_im = Image.fromarray(dis_array)
    new_im.show()
# sigmoid函数
def sigmoid(z):
    return 1/(1+exp(-z))
# 辅助函数-添加ones((m,1))
def help_addones(x):
    m,n = shape(x)
    x = np.array(x)
    ret = []
    for i in range(m):
        tmp = [1]
        for j in range(n):
            tmp.append(x[i][j])
        ret.append(tmp)
    return ret 
# 利用已有weight做预测
def predict(theta1,theta2,x):
    m = shape(x)[0]
    num_labels = shape(theta2)[0]
    p = zeros((m,1))

    a1 = mat(help_addones(x))
    z2 = a1 * mat(theta1).T 
    a2 = sigmoid(z2)
    a2 = mat(help_addones(a2))
    z3 = a2 * mat(theta2).T 
    a3 = sigmoid(z3)

    a3 = np.array(a3).tolist()
    ret = []
    for i in range(m):
        ret.append(a3[i].index(max(a3[i])) + 1)
    return ret
# 计算错误率
def calcAcc(y,pre):
    m,n = shape(y)
    y = np.array(y);
    pre = np.array(pre).reshape((m,n));
    print(shape(y))
    print(shape(pre))
    print(y[0],pre[0])
    err = 0
    for i in range(m):
        if y[i][0] != pre[i][0]:
            err += 1
    print("正确率: ",(1 - float(err)/m)*100,'%')
# 实时显示预测的例子
def test(x,Theta1,Theta2):
    m,n = shape(x)
    for i in range(m):
        print('显示图片')
        randIdx = random.randint(0,m)
        displayData(x[randIdx:randIdx+1])
        pre = predict(Theta1,Theta2,x[randIdx:randIdx+1])
        print('预测的值是: ',pre)
        s = input('Paused - press enter to continue, q to exit:')
        if s == 'q':
            break 

# 主函数
def main():
    input_layer_size = 400
    hidden_layer_size = 25
    num_labels = 10
    print('************************************************')
    x,y = loadData('ex3data1.mat')
    m,n = shape(x)
    Theta1,Theta2 = loadWight('ex3weights.mat')
    print('************************************************')
    idx = [random.randint(0,m) for _ in range(100)]
    # displayData(x[idx])
    print('************************************************')
    pred = predict(Theta1,Theta2,x)
    calcAcc(y,pred)
    print('************************************************')
    test(x,Theta1,Theta2)

main()  