import scipy.io as sio  
import matplotlib.pyplot as plt  
import numpy as np  
from numpy import *
from PIL import Image
import random 

# 加载数据
def loadData(filename):
    data = sio.loadmat(filename)
    x = data['X']
    y = data['y']
    print(shape(x))
    print(shape(y))
    return x,y 
# 加载文件
def loadFile(filename):
    x = []
    n = len(open(filename).readline().strip().split(' '))
    fp =  open(filename)
    for line in fp.readlines():
        line = line.strip().split(' ')
        tmp = []
        for i in range(n):
            tmp.append(float(line[i]))
        x.append(tmp)
    return mat(x).T
# 预处理一下数据
def prepareX(x):
    m,n = shape(x)
    for i in range(m):
        for j in range(n):
            x[i,j] = floor((x[i,j] + 0.2) * 180)
    return x
# 画图函数展示数据
def displayData(x):
    m,n = shape(x);
    h = int(sqrt(n))
    w = h;
    row = int(sqrt(m))
    col = row;

    pad = 1
    dis_array = zeros((pad + row * (h + pad),pad + col * (w + pad)));
    x = prepareX(x)
    k = 0
    m,n = shape(dis_array)
    for i in range(m):
        for j in range(n):
            dis_array[i,j] = 255
    for i in range(row): 
        for j in range(col):
            dis_array[i*h +(i+1)*pad:(i+1)*h +(i+1)*pad,j*w +(j+1)*pad:(j+1)*w +(j+1)*pad] = reshape(x[k],(h,w))
            k += 1

    new_im = Image.fromarray(dis_array)
    new_im.show()
# sigmoid函数
def sigmoid(z):
    return 1/(1+exp(-z))
# 计算梯度与Cost
def computeCost(x,y,theta,lamda):
    m,n = shape(x)
    x = mat(x); y = mat(y); theta = mat(theta)
    J = 1/m*sum(multiply(-y,log(sigmoid(x * theta))) - multiply(1-y,log(1-sigmoid(x * theta)))) \
        + lamda/(2*m)*(theta.T * theta - theta[0] * theta[0])
    
    grad = zeros((shape(theta)))
    grad = 1/m*x.T * (sigmoid(x * theta) - y) + lamda / m * theta
    grad[0] = grad[0] - lamda/m*theta[0]
    grad = grad[:]
    return J,grad 
##################################################
# 梯度下降找最优theta
def gradientDescent(x,y,theta,alpha,Iters,k):
    m = len(y)
    x = mat(x); y = mat(y).T; theta = mat(theta)
    for iters in range(Iters):
        if iters % 1000 == 0:
            print('第',k,': 迭代 ',iters,' 次');
        theta = theta - alpha*(x.T * (sigmoid(x * theta) - y))
    return theta.T

# 训练多元分类器
def oneVsAll(x,y,num_labels,lamda):
    m,n = shape(x)
    all_theta = zeros((num_labels,n+1))
    x = help_addones(x)

    for i in range(num_labels):
        tmp_theta = zeros((n+1,1))
        tmp_y = []
        for j in range(m):
            if y[j] == i: tmp_y.append(1)
            else: tmp_y.append(0)
        tmp = gradientDescent(x,tmp_y,tmp_theta,0.001,500000,i)
        all_theta[0] = tmp
    print(shape(all_theta))
    return all_theta
##################################################

# 辅助函数-添加ones((m,1))
def help_addones(x):
    m,n = shape(x)
    x = np.array(x)
    ret = []
    for i in range(m):
        tmp = [1]
        for j in range(n):
            tmp.append(x[i][j])
        ret.append(tmp)
    return ret 
# 做预测
def predict(x,all_theta):
    m,n = shape(x)
    x = mat(help_addones(x))
    h = x * mat(all_theta)
    h = np.array(h).tolist()

    ret = []
    for i in range(m):
        ret.append(h[i].index(max(h[i])) + 1)
    return ret
# 计算错误率
def calcAcc(y,pre):
    m,n = shape(y)
    y = np.array(y);
    pre = np.array(pre).reshape((m,n));
    err = 0
    for i in range(m):
        if y[i][0] != pre[i][0]:
            err += 1
    print("正确率: ",(1 - float(err)/m)*100,'%')

# 主函数
def main():
    num_labels = 10 
    x,y = loadData('ex3data1.mat')
    m,n = shape(x)
    print('*****************************************')
    idx = [random.randint(0,m) for _ in range(100)]
    # displayData(x[idx])
    print('*****************************************')
    theta_t = [[-2],[-1],[1],[2]]
    x_t = [
        [1,0.1,0.6,1.1],
        [1,0.2,0.7,1.2],
        [1,0.3,0.8,1.3],
        [1,0.4,0.9,1.4],
        [1,0.5,1.0,1.5]
    ]
    y_t = [[1],[0],[1],[0],[1]]
    lamda_t = 3
    [J,grad] = computeCost(x_t,y_t,theta_t,lamda_t)
    print('cost: ',J,' (2.534819)')
    print('grad: ',grad)
    print('(0.146561 -0.548558 0.724722 1.398003)')

    print('*****************************************')
    all_theta = loadFile('all_theta.mat')
    pre = predict(x,all_theta)
    calcAcc(y,pre)

    print('*****************************************')
    # all_theta = oneVsAll(x,y,num_labels,0)
    # pre = predict(x,mat(all_theta).T)
    # calcAcc(y,pre)

main()