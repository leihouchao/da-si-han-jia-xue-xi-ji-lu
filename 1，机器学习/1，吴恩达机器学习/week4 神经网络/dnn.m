clear; close all; clc 

input_layer_size = 400 ;
hidden_layer_size = 25;
num_labels = 10;

% 加载数据
load('ex4data1.mat')
m = size(X,1);

% 显示图像
sel = randperm(size(X, 1));
sel = sel(1:100);
% 画图函数
function [h,display_array] = displayData(X)
    example_width = round(sqrt(size(X,2)));
    colormap(gray);
    [m,n] = size(X);
    example_height = n/example_width;

    display_row = floor(sqrt(m));
    display_col = ceil(m/display_row);

    pad = 1;
    display_array = -ones(pad + display_row * (example_height + pad), ...
                          pad + display_col * (example_width + pad));
    curr_ex = 1;
    for j = 1:display_row
        for i = 1:display_col
            if curr_ex > m
                break;
            end
            max_val = max(abs(X(curr_ex,:)));
            display_array(pad + (j - 1) * (example_height + pad) + (1:example_height), ...
                    pad + (i - 1) * (example_width + pad) + (1:example_width)) = ...
                    reshape(X(curr_ex, :), example_height, example_width) / max_val;
		    curr_ex = curr_ex + 1;
        end
        if curr_ex > m 
            break;
        end 
    end

    h = imagesc(display_array, [-1 1]);
    axis image off;
    drawnow;
end
% sigmoid函数
function h = sigmoid(z)
    h = 1./(1 + exp(-z));
end 

%displayData(X(sel,:));

% 加载权重
load('ex4weights.mat')
w = [Theta1(:); Theta2(:)];
lamda = 0 ;

% 计算sigmoid的梯度
function g = sigmoidGradient(z)
    g = zeros(size(z));
    g = sigmoid(z).*(1-sigmoid(z));
end

% 计算Cost与梯度
function [J,grad] = nnComputeCost(nn_params,input_layer_size,hidden_layer_size,num_labels,x,y,lamda)
    % 先将参数由一维转成矩阵
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)),...
                    hidden_layer_size,input_layer_size + 1
                    );
    Theta2 = reshape(nn_params(1 + hidden_layer_size * (input_layer_size + 1):end),...
                    num_labels,1 + hidden_layer_size
                    );
    m = size(x,1);
    J = 0;
    % theta导数的维数与theta一样
    Theta1_grad = zeros(size(Theta1));
    Theta2_grad = zeros(size(Theta2));

    % 将向量y转换成矩阵
    Y = zeros(m,num_labels);
    for i = 1:m
        Y(i,y(i)) = 1;
    end

    % 正向传播计算a值
    x = [ones(m,1) x];
    a2 = sigmoid(x * Theta1');  %'
    a2 = [ones(m,1) a2];
    a3 = sigmoid(a2 * Theta2') ; %'

    % 计算J
    J = 1/m * sum(sum(-Y .* log(a3) - (1-Y).*log(1-a3)));
     
    % 因为正则化不计算theta1(1)和theta2(1),所以先去除,再计算
    theta1 = Theta1(:,2:end);
    theta2 = Theta2(:,2:end);
    J = J + lamda/(2*m)*(sum(sum(theta1.*theta1)) + sum(sum(theta2.*theta2)));

    % 计算梯度
    tmpDelta1 = zeros(size(Theta1));
    tmpDelta2 = zeros(size(Theta2));
    for i = 1:m 
        % 首先进行前向计算
        a1 = x(i,:)'; %'
        z2 = Theta1 * a1;
        a2 = sigmoid(z2);
        a2 = [1;a2];
        z3 = Theta2 * a2 ;
        a3 = sigmoid(z3);

        % 计算反向导数
        delta3 = a3 - Y(i,:)'; %'
        delta2 = ((Theta2' * delta3) .* sigmoidGradient([1;z2]))(2:end); %'

        tmpDelta1 = tmpDelta1 + delta2 * a1'; %'
        tmpDelta2 = tmpDelta2 + delta3 * a2'; %'
    end 
    Theta1_grad = tmpDelta1 ./ m ;
    Theta2_grad = tmpDelta2 ./ m ;
    % 正则化梯度
    Theta1_grad(:,2:end) = Theta1_grad(:,2:end) + lamda/m*Theta1(:,2:end) ;
    Theta2_grad(:,2:end) = Theta2_grad(:,2:end) + lamda/m*Theta2(:,2:end) ;
    % 展开成一维向量
    grad = [Theta1_grad(:); Theta2_grad(:)];

end

J = nnComputeCost(w,input_layer_size,hidden_layer_size,num_labels,X,y,lamda);
fprintf('cost: %f (0.287629)\n',J)
% 加入正则化
lamda = 1;
J = nnComputeCost(w,input_layer_size,hidden_layer_size,num_labels,X,y,lamda);
fprintf('cost: %f (0.383770)\n',J)

lamda = 3;
J = nnComputeCost(w,input_layer_size,hidden_layer_size,num_labels,X,y,lamda);
fprintf("cost: %f (0.576051)\n",J);


% 随机初始化权重
function w = randInitW(L_out,L_in)
    w = zeros(L_out,1 + L_in);
    epo_init = 0.12 ;
    w = rand(L_out,1 + L_in) * 2 * epo_init - epo_init ;
end
init_theta1 = randInitW(hidden_layer_size,input_layer_size);
init_theta2 = randInitW(num_labels, hidden_layer_size);
init_w = [init_theta1(:); init_theta2(:)];

% 梯度校验
function w = debugInitW(Lout,Lin)
    w = zeros(Lout,1 + Lin);
    w = reshape(sin(1:numel(w)),size(w))/10;
end
function numgrad = computerNumGradient(J,theta)
    numgrad = zeros(size(theta));
    epo = zeros(size(theta));
    e = 1e-4 ;
    for p = 1:numel(theta)
        epo(p) = e;
        loss1 = J(theta - epo);
        loss2 = J(theta + epo);
        numgrad(p) = (loss2 - loss1) / (2*e);
        epo(p) = 0;
    end
end
function checkNNGradient(lamda)
    % 测试网络
    input_layer_size = 3;
    hidden_layer_size = 5;
    num_labels = 3;
    m = 5;

    % 随机初始化x,y,w 
    Theta1 = debugInitW(hidden_layer_size,input_layer_size);
    Theta2 = debugInitW(num_labels,hidden_layer_size);
    x = debugInitW(m,input_layer_size-1);
    y = 1 + mod(1:m,num_labels)'; %'

    % 展开参数
    nn_params = [Theta1(:); Theta2(:)];

    % 计算 反向传播的梯度
    costFunc = @(p) nnComputeCost(p, input_layer_size, hidden_layer_size, ...
                               num_labels, x, y, lamda);
    [cost,grad] = costFunc(nn_params);

    % 计算数值梯度
    numgrad = computerNumGradient(costFunc,nn_params);

    % 输出比较
    disp([numgrad grad]);
    diff = norm(numgrad - grad) / norm(numgrad + grad);
    fprintf('less than:%f (1e-9)\n',diff);

end

% checkNNGradient(0);
% checkNNGradient(3);

% 使用计算好的cost与梯度,用优化函数fmincg训练网络,计算最优参数
option = optimset('MaxIter',50);
lamda = 1;
costFunc = @(p) nnComputeCost(p,input_layer_size,hidden_layer_size,num_labels,X,y,lamda);
[nn_params,cost] = fmincg(costFunc,init_w,option);

% 可视化训练结果
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));
Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));
displayData(Theta1(:, 2:end));

% 利用训练好的参数做预测
function p = predict(theta1,theta2,x)
    m = size(x,1);
    num_labels = size(theta2,1);
    p = zeros(m,1);

    h1 = sigmoid([ones(m,1) x] * theta1') ; %'
    h2 = sigmoid([ones(m,1) h1] * theta2'); %'

    [~,p] = max(h2,[],2);
end

pre = predict(Theta1,Theta2,X);
fprintf("acc: %f\n",mean(double(pre == y))* 100);