from numpy import *
import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

# 加载数据
def loadData(filename):
    x = []; y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split(',')
        x.append([float(line[0]),float(line[1])])
        y.append(float(line[2]))
    x = mat(x); y = mat(y).T
    return x,y
# 画图
def plotData(x,y):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    x1 = []; y1 = []
    x0 = []; y0 = []
    m,n = shape(x)
    for i in range(m):
        if(y[i][0] == 1):
            x1.append(x[i,0])
            y1.append(x[i,1])
        else:
            x0.append(x[i,0])
            y0.append(x[i,1])
    ax.scatter(x0,y0)
    ax.scatter(x1,y1)
    plt.show()
# sigmoid函数
def sigmoid(z):
    h = zeros((shape(z)))
    h = 1/(1 + exp(-z))
    return h 
# 特征映射
def featureMap(x1,x2):
    degrees = 6 
    x1 = mat(x1)
    out = ones((shape(x1)[0],28))
    x1 = np.array(x1)
    x2 = np.array(x2)
    k = 0
    for i in range(degrees + 1):
        for j in range(0,i+1):
            tmp = multiply(pow(x1,i-j),pow(x2,j))
            tmp = tmp[:,0]
            out[:,k] = tmp
            k += 1
    return out 
# 带有正则化的计算cost函数
def computeCost(x,y,theta,lamda):
    m,n = shape(x)
    x = mat(x); y = mat(y); theta = mat(theta)
    J = 1/m * sum(multiply(-y,log(sigmoid(x * theta))) - multiply(1-y,log(1-sigmoid(x * theta)))) \
        + lamda/(2 * m) * (theta.T * theta - theta[0] * theta[0])
    grad = zeros((n,1))
    grad = 1/m * (x.T * (sigmoid(x * theta) - y)) + lamda/m * theta 
    grad[0] = grad[0] - lamda / m * theta[0]
    return J,grad 
# 画决策边界
def plotDecBound(x,y,theta):
    # 画点
    x = mat(x); y =mat(y);
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    x1 = []; y1 = []
    x0 = []; y0 = []
    m,n = shape(x)
    for i in range(m):
        if(y[i][0] == 1):
            x1.append(x[i,1])
            y1.append(x[i,2])
        else:
            x0.append(x[i,1])
            y0.append(x[i,2])
    ax.scatter(x0,y0)
    ax.scatter(x1,y1)
    # 画线
    u = np.arange(-1,1.5,0.05)
    v = np.arange(-1,1.5,0.05)
    z = zeros((len(u),len(v)))
    for i in range(len(u)):
       for j in range(len(v)):
           tmp = mat(featureMap(u[i],v[j])) * mat(theta)
           z[i,j] = tmp
    z = z.T 
    u,v = np.meshgrid(u,v)

    plt.contour(u,v,z,[0])
    plt.show()
# 梯度下降找最优theta
def gradientDescent(x,y,theta,alpha,Iters):
    m = len(y)
    x = mat(x); y = mat(y); theta = mat(theta)
    for iters in range(Iters):
        theta = theta - alpha*(x.T * (sigmoid(x * theta) - y))
    return theta 
# 预测函数
def predict(x,theta):
    m,n = shape(x)
    p = zeros((m,1))
    x = mat(x); theta = mat(theta)
    p = (sigmoid(x * theta) >= 0.5)
    return p 
# 计算错误率
def calcAcc(y,pre):
    m,n = shape(y)
    err = 0
    for i in range(m):
        if y[i] != pre[i]:
            err += 1
    print("正确率: ",1 - float(err)/m)

# 主函数
def main():
    x,y = loadData('ex2data2.txt')
    # plotData(x,y)
    print('===========================================\n')
    print(shape(x))
    x = featureMap(x[:,0],x[:,1])
    print(shape(x))

    print('===========================================\n')
    theta = zeros((shape(x)[1],1))
    lamda = 1
    [cost,grad] = computeCost(x,y,theta,lamda)
    print('cost: ',cost,' (0.693)')
    print('grad: ',grad[0:5])
    print(' (0.0085 ,0.0188, 0.0001, 0.0503, 0.0115)')

    print('===========================================\n')
    test_theta = ones((shape(x)[1],1))
    [cost,grad] = computeCost(x,y,test_theta,10)
    print('cost: ',cost,' (3.16)')
    print('grad: ',grad[0:5])
    print(' (0.3460,  0.1614,  0.1948,  0.2269,  0.0922)')

    print('===========================================\n')
    alpha = 0.001 
    Iters = 500000
    theta = zeros((28,1))
    theta = gradientDescent(x,y,theta,alpha,Iters)
    print('theta: ',theta)
    plotDecBound(x,y,theta)

    print('===========================================\n')
    pre = predict(x,theta)
    calcAcc(y,pre)

main()