from numpy import *
import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

# 加载数据
def loadData(filename):
    x = []; y = []
    fp = open(filename)
    for line in fp.readlines():
        line = line.strip().split(',')
        x.append([float(line[0]),float(line[1])])
        y.append(float(line[2]))
    x = mat(x); y = mat(y).T
    return x,y
# 画图
def plotData(x,y):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    x1 = []; y1 = []
    x0 = []; y0 = []
    m,n = shape(x)
    for i in range(m):
        if(y[i][0] == 1):
            x1.append(x[i,0])
            y1.append(x[i,1])
        else:
            x0.append(x[i,0])
            y0.append(x[i,1])
    ax.scatter(x0,y0)
    ax.scatter(x1,y1)
    plt.show()
# sigmoid函数
def sigmoid(z):
    h = zeros((shape(z)))
    h = 1/(1 + exp(-z))
    return h 
# 计算cost函数
def computeCost(x,y,theta):
    m = shape(x)[0]
    x = mat(x); y = mat(y); theta = mat(theta)
    J = -1/m * sum(multiply(y , log(sigmoid(x * theta))) 
                 + multiply((1 - y),log(1 - sigmoid(x * theta)))
                ) 
    grad = 1/m * (x.T * (sigmoid(x * theta) -y))
    return J,grad 
# 辅助函数
def help_addones(x):
    m,n = shape(x)
    x = np.array(x)
    ret = []
    for i in range(m):
        tmp = []
        tmp = [1,x[i][0],x[i][1]]
        ret.append(tmp)
    return ret 
# 画决策边界
def plotDecBound(x,y,theta):
    # 画点
    x = mat(x); y =mat(y);
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    x1 = []; y1 = []
    x0 = []; y0 = []
    m,n = shape(x)
    for i in range(m):
        if(y[i][0] == 1):
            x1.append(x[i,1])
            y1.append(x[i,2])
        else:
            x0.append(x[i,1])
            y0.append(x[i,2])
    ax.scatter(x0,y0)
    ax.scatter(x1,y1)
    # 画线
    plot_x = np.arange(min(x[:,1]) - 2, max(x[:,1]) + 2,0.1)
    plot_y = multiply(-1/theta[2][0],multiply(theta[1][0],plot_x) + theta[0][0]).T;
    print(shape(plot_x))
    print(shape(plot_y))
    ax.plot(plot_x,plot_y,'r--')
    plt.show()

# 梯度下降找最优theta
def gradientDescent(x,y,theta,alpha,Iters):
    m = len(y)
    x = mat(x); y = mat(y); theta = mat(theta)
    for iters in range(Iters):
        theta = theta - alpha*(x.T * (sigmoid(x * theta) - y))
    return theta 

# 主函数
def main():
    x,y = loadData('ex2data1.txt')
    # plotData(x,y)
    print(shape(x))
    print('======================================')
    m,n = shape(x)
    x = help_addones(x)
    print(shape(x))
    theta = zeros((n+1,1))
    [cost,grad] = computeCost(x,y,theta)
    print('cost: ',cost,' (0.693)')
    print('grad: ',grad,'')
    print('-0.1,-12.0092,-11.2628')
    print('======================================')
    test_theta = [[-24],[0.2],[0.2]]
    [cost,grad] = computeCost(x,y,test_theta)
    print('cost: ',cost,' (0.218)')
    print('grad: ',grad,'')
    print('0.043,2.566,2.647')
    print('======================================')
    alpha = 0.001 
    Iters = 500000
    theta = zeros((3,1))
    theta = gradientDescent(x,y,theta,alpha,Iters)
    print('theta: ',theta)
    plotDecBound(x,y,theta)

 
main()  
