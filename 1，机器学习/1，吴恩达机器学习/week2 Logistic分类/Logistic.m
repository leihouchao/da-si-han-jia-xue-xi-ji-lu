clear;close all; clc

% 加载数据
data = load('ex2data1.txt');
x = data(:,[1,2]);
y = data(:,3);

% 可视化数据
function plotData(X,y)
    figure; hold on;
    pos = find(y == 1);
    neg = find(y == 0);
    plot(X(pos,1),X(pos,2),'k+','LineWidth',2,'MarkerSize',7);
    plot(X(neg,1),X(neg,2),'ko','MarkerFaceColor','y','MarkerSize',7);
    hold off;
end  
plotData(x,y)

% sigmoid函数
function h = sigmoid(z)
    h = zeros(size(z));
    h = 1./(1 + exp(-z));
end

% 计算cost
function [J,grad] = computeCost(x,y,theta)
    m = length(y);
    J = -1/m * sum(y.*log(sigmoid(x * theta)) + (1-y).* log(1-sigmoid(x*theta)));
    grad = 1/m*(x' * (sigmoid(x * theta) - y)); %'
end
[m,n] = size(x);
x = [ones(m,1) x];
theta = zeros(n+1,1);
[cost,grad] = computeCost(x,y,theta);
close all;
fprintf('cost:%f(0.693) \n',cost);
fprintf('grad:\n');
fprintf('%f\n',grad)
fprintf("-0.1,-12.0092,-11.2628\n");

test_theat = [-24;0.2;0.2];
[cost,grad] = computeCost(x,y,test_theat);
fprintf("cost:%f(0.218)\n",cost);
fprintf("grad:\n");
fprintf('%f\n',grad);
fprintf("0.043,2.566,2.647\n");

% 画决策边界
function plotDecBoundary(x,y,theta,theta2)
    close all;
    plotData(x(:,2:3),y)
    hold on;
    plot_x = [min(x(:,2))-2,max(x(:,2))+2];
    plot_y = (-1./theta(3)).*(theta(2).*plot_x + theta(1));
    plot(plot_x,plot_y,'r-')
    hold off;
end
function plotDecBoun(x,y,theta,theta2)
    close all;
    plotData(x(:,2:3),y)
    hold on;
    plot_x = [min(x(:,2))-2,max(x(:,2))+2];
    plot_y = (-1./theta(3)).*(theta(2).*plot_x + theta(1));
    plot_y2 = (-1./theta2(3)).*(theta2(2).*plot_x + theta2(1));
    %// fprintf("theta1: %f\n",theta)
    %// fprintf("theta2: %f\n",theta2)

    plot(plot_x,plot_y,'r-')
    plot(plot_x,plot_y2,'g-')
    hold off;
end

% 使用优化算法计算最优theta
theta = zeros(3,1);
options = optimset('GradObj', 'on', 'MaxIter', 400);
[theta, cost] = ...
	fminunc(@(t)(computeCost( x, y,t)), theta, options);
plotDecBoundary( x, y,theta,[]);

% 做预测
close all;
clc;
p = sigmoid([1 45 85] * theta)
fprintf('p = %f(0.775)\n',p)
% 预测函数
function p = predict(x,theta)
    m = size(x,1);
    p = zeros(m,1);
    p = (sigmoid(x*theta) >= 0.5);
end
p = predict(x,theta);
fprintf('acc:%f(89.0)\n',mean(double(p == y))*100)
fprintf('==================================================\n')
fprintf('==================================================\n')
fprintf('==================================================\n')
%//************************************************************************
% 梯度下降算法
function theta = gradientDescent(x,y,theta,alpha,Iters)
    m = length(y);
    for iters = 1:Iters
        theta = theta - alpha * (x' * (sigmoid(x * theta) - y)); %'
    end
end
close all;
alpha = 0.001;
Iters = 500000;                % 迭代50万次之后,分类效果好
theta = zeros(3,1);
theta = gradientDescent(x,y,theta,alpha,Iters);
fprintf("theta:\n");
fprintf("%f\n",theta);
fprintf("-25.161 0.206 0.201\n");
theta2 = [-25.161 0.206 0.201];
plotDecBoun( x, y,theta,theta2);
