clear;close all;clc 

% 加载数据
data = load('ex2data2.txt');
x = data(:,[1,2]);
y = data(:,3);
% 画图
function plotData(x,y)
    figure;
    hold on;
    pos = find(y == 1);
    neg = find(y == 0);
    plot(x(pos,1),x(pos,2),'k+','LineWidth',2,'MarkerSize',7);
    plot(x(neg,1),x(neg,2),'ko','MarkerFaceColor','y','MarkerSize',7);
    hold off ;
end

% plotData(x,y);

% 特征映射
function out = featureMap(x1,x2)
    degree = 6 ;
    out = ones(size(x1(:,1)));
    for i = 1:degree
        for j = 0:i
            out(:,end+1) = (x1.^(i-j)).*(x2.^j);
        end
    end
end

x = featureMap(x(:,1),x(:,2));
theta = zeros(size(x,2),1);
lambda = 1 ;

% sigmoid 函数
function h =sigmoid(z)
    h = zeros(size(z));
    h = 1./(1 + exp(-z));
end
% 计算cost
function [J,grad] = computeCost(theta,x,y,lambda)
    m = length(y);
    J = 0 ;
    grad = zeros(size(theta,1));
    J = 1/m*sum(-y.*log(sigmoid(x * theta)) - (1-y).*(log(1-sigmoid(x*theta))) 
        + lambda/(2*m)*(theta' * theta - theta(1) * theta(1))) ;   %'
    grad = 1/m * (x' * (sigmoid(x * theta) - y)) + lambda/m*theta ;    %'
    grad(1) = grad(1) - lambda/m*theta(1);
end

[cost,grad] = computeCost(theta,x,y,lambda);
fprintf("cost:%f(0.693)\n",cost);
fprintf("grad: %f\n",grad(1:5));
fprintf(' (0.0085,0.0188,0.0001,0.0503,0.0115)\n');

test_theta = ones(size(x,2),1);
[cost,grad] = computeCost(test_theta,x,y,10);
fprintf("cost:%f(3.16)\n",cost);
fprintf("grad: %f\n",grad(1:5));
fprintf("0.3460 0.1614 0.1948 0.2269 0.0922\n");

fprintf('================ 1 ==========================\n')

% 利用优化函数来解theta*
theta = zeros(size(x,2),1);
lambda = 1;
options =  optimset('GradObj', 'on', 'MaxIter', 400);
fprintf('================ 2 ==========================\n');
[theta, J, exit_flag] = ...
	fminunc(@(t)(computeCost( t,x, y, lambda)), theta, options);
fprintf('================ 3 ==========================\n');

% 画决策边界
function plotDecBoundary(x,y,theta)
    plotData(x(:,2:3),y);
    hold on ;
    fprintf('============== 4 ============================\n')
    if size(x,2) <= 3
        fprintf('==============5============================\n')
        plot_x = [min(x(:,2)) - 2,max(x(:,2))+2];
        plot_y = (-1./theat(3).*(theat(2).*plot_x + theta(1)));
        plot(plot_x,plot_y);
    else
        fprintf('============== 6 ============================\n')
        u = linspace(-1,1.5,50);
        v = linspace(-1,1.5,50);
        z = zeros(length(u),length(v));
        fprintf('============== 8 ============================\n')
        for i = 1:length(u)
            for j = 1:length(v)
                z(i,j) = featureMap(u(i),v(j)) * theta;
            end 
        end
        fprintf('============== 7 ============================\n')
        z = z' ;   % '
        contour(u,v,z,[0,0],'LineWidth',2);
    end
    hold off ;
end
fprintf('theta: %f\n', theta)
plotDecBoundary(x,y,theta);

% 预测函数
function p = predict(x,theta)
    m = size(x,1);
    p = zeros(m,1);
    p = (sigmoid(x * theta) >= 0.5);
end
% p = predict(x,theta);
% fprintf('acc: %f(83.1)\n',mean(double(p == y)));