clear; close all; clc
 
%%========================== part 1 =========================
% 加载数据
load('multiGuassianData.mat');
% 可视化数据
plot(X(:,1),X(:,2),'bx');
axis([0 30 0 30]);


%======================= part 2 =====================
% 计算多元高斯每一维的 均值和方差
function [mu,sigma] = computeMean(x)
    [m,n] = size(x);
    mu = zeros(n,1);
    sigma = zeros(n,1);

    mu = mean(x)'; %'
    sigma = sum((x - mu').^2) / m ; %'
end
% 多元高斯混合模型: 计算每个样本正常的概率
function p = multiGuassianCompute(x,mu,sigma)
    n = size(x,2);
    if size(sigma,1) == 1 || size(sigma,2) == 1
        sigma = diag(sigma);
    end
    x = x - mu';  %'
    p = (2 * pi)^(-n/2) * det(sigma)^(0.5) * exp(-0.5 * sum(x * pinv(sigma).*x,2));
end
[mu sigma] = computeMean(X);
fprintf('mu = %f \n',mu)
fprintf('sigma = %f \n',sigma)

p = multiGuassianCompute(X,mu,sigma);
fprintf('size: %d\n',size(p))
fprintf('size: %d\n',size(X))

% 绘图函数: 画等高线
function visiualize(x,mu,sigma)
    [x1,x2] = meshgrid(0:0.5:35);
    tmp = [x1(:) x2(:)];
  
    z = multiGuassianCompute([x1(:) x2(:)],mu,sigma);  
    z = reshape(z,size(x1));

    %// fprintf("min(z) = %f\n",min(z))
    fprintf("%d \n",size(z))
    fprintf('%f ',z(1,:))

    plot(x(:,1),x(:,2),'bx');
    hold on;
    if  sum(isinf(z)) == 0
        contour(x1,x2,z,10.^(-20:3:0)'); %'
    end
    hold off;
end
visiualize(X,mu,sigma)

%===================== part 3 =============================
% 找出异常点
pval = multiGuassianCompute(Xval,mu,sigma);

% 利用有Label的数据进行交叉验证,挑选出最好的阈值
function [bestEpsion bestF1] = selectThreshold(y,pval)
    bestEpsion = 0;
    bestF1 = 0;
    F1 = 0;

    stepSize = (max(pval) - min(pval)) / 1000;
    for epsion = min(pval):stepSize:max(pval)
        pre = (pval < epsion);
        tp = sum((pre == 1) & (y == 1));
        fp = sum((pre == 1) & (y == 0));
        fn = sum((pre == 0) & (y == 1));
        
        % 计算召回率与准确率
        if tp + fp != 0
            prec = tp/(tp+fp);
        else
            prec = 0;
        end 
        if tp + fn != 0
            rec = tp/(tp+fn);
        else
            rec = 0;
        end 
        
        % 计算F1
        if prec != 0 && rec != 0
            F1 = 2*prec*rec/(prec + rec)    ;
        else
            F1 = 0;
        end

        if F1 > bestF1
            bestF1 = F1 ;
            bestEpsion = epsion;
        end
    end
end

[epsion,F1] = selectThreshold(yval,pval);
fprintf('Best epsilon found using cross-validation: %e\n', epsion);
fprintf('Best F1 on Cross Validation Set:  %f\n', F1);
fprintf('   (you should see a value epsilon of about 8.99e-05)\n');
fprintf('   (you should see a Best F1 value of  0.875000)\n\n');

% 圈出异常点
outliers = find(p < epsion);
hold on;
plot(X(outliers,1),X(outliers,2),'ko','LineWidth',2,'MarkerSize',10);
hold off;

%==================== part 4 ===================
% 多元高斯模型用于 高维
load('multiGuassianData2.mat');
[mu,sigma] = computeMean(X);
p = multiGuassianCompute(X,mu,sigma);
pval = multiGuassianCompute(Xval,mu,sigma);
[eposion F1] = selectThreshold(yval,pval);

fprintf('Best epsilon found using cross-validation: %e\n', eposion);
fprintf('Best F1 on Cross Validation Set:  %f\n', F1);
fprintf('   (you should see a value epsilon of about 1.38e-18)\n');
fprintf('   (you should see a Best F1 value of 0.615385)\n');
fprintf('# Outliers found: %d\n\n', sum(p < eposion));
