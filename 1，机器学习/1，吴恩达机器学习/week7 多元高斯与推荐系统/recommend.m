close all; clear; clc

%% ==================== part 1 =========================
% 加载数据
load('recommendData_movies.mat');
fprintf("movie 1 score: %f\n",mean(Y(1,R(1,:))));
% whos;

%% ===================== part 2 =========================
% 协同过滤算法计算Cost函数
load('ex8_movieParams.mat');
num_users = 4;
num_movies = 5;
num_feat = 3;
x = X(1:num_movies,1:num_feat);
theta = Theta(1:num_users,1:num_feat);
y = Y(1:num_movies,1:num_users);
r = R(1:num_movies,1:num_users);

% cost函数: 计算Cost与梯度
function [J,grad] = computeCost(params,y,r,num_users,num_movies,num_feat,lamda)
    % 电影矩阵 num_movies * k
    x = reshape(params(1:num_movies * num_feat),num_movies,num_feat);
    % 用户矩阵 num_users * k
    theta = reshape(params(num_movies * num_feat + 1: end),num_users,num_feat);

    J = 0;
    x_grad = zeros(size(x));
    theta_grad = zeros(size(theta));

    % 评分矩阵 
    errNum = (x * theta' - y).* r; %';
    J = 0.5 * sum(sum(errNum.^2)) + lamda/2*sum(sum(theta.^2)) + lamda/2*sum(sum(x.^2));
    x_grad = errNum * theta + lamda * x ;
    theta_grad = errNum' * x + lamda * theta; %'

    grad = [x_grad(:); theta_grad(:)];
end

J = computeCost([x(:);theta(:)],y,r,num_users,num_movies,num_feat,0);
fprintf("J = %f (22.22)\n",J)

% ================== part 3 =======================
% 梯度校验
function numgrad = computeNumGrad(J,theta)
    numgrad = zeros(size(theta));
    item = zeros(size(theta));
    e = 1e-4;
    for p = 1:numel(theta)
        item(p) = e;
        loss1 = J(theta - item);
        loss2 = J(theta + item);
        numgrad(p) = (loss2 - loss1) / (2*e);
        item(p) = 0;
    end
end
function checkGradient(lamda)
    % 初始化数据
    x_t = rand(4,3);
    theta_t = rand(5,3);
    y = x_t * theta_t'; %'
    y(rand(size(y)) > 0.5) = 0;
    r = zeros(size(y));
    r(y ~= 0 ) = 1;

    x = randn(size(x_t));
    theta = randn(size(theta_t));
    num_users = size(y,2);
    num_movies = size(y,1);
    num_feat = size(theta_t,2);

    numgrad = computeNumGrad( ...
            @(t) computeCost(t,y,r,num_users,num_movies,...
                num_feat,lamda),[x(:); theta(:)]);
    [cost,grad] = computeCost([x(:);theta(:)],y,r,num_users,...
                num_movies,num_feat,lamda);
            
    disp([numgrad grad])
    diff = norm(numgrad - grad) / norm(numgrad + grad);
    fprintf("dif should be small(1e-9):%f\n",diff);
end

% checkGradient(0)
% 测试正则化Cost值
J = computeCost([x(:); theta(:)],y,r,num_users,num_movies,num_feat,1.5);
fprintf(['Cost at loaded parameters (lambda = 1.5): %f '...
         '\n(this value should be about 31.34)\n'], J);
% checkGradient(1.5);

%================== part 4 =====================
% 加载电影名称
function movieList = loadMovieName()
    fp = fopen('movie_ids.txt');
    n = 1682;
    movieList = cell(n,1);

    for i = 1:n 
        line = fgets(fp);
        [idx,movieName] = strtok(line,' ');
        movieList{i} = strtrim(movieName);
    end

    fclose(fp);
end
movieList = loadMovieName();

% 新增一个用户及其评价
my_ratings = zeros(1682,1);
my_ratings(1) = 4;
my_ratings(98) = 2;
my_ratings(7) = 3;
my_ratings(12)= 5;
my_ratings(54) = 4;
my_ratings(64)= 5;
my_ratings(66)= 3;
my_ratings(69) = 5;
my_ratings(183) = 4;
my_ratings(226) = 5;
my_ratings(355)= 5;

% ====================== part 5 ===========================
% 训练推荐系统
load('recommendData_movies.mat');
y = [my_ratings Y];
r = [(my_ratings ~= 0) R];
% 数据归一化
function [ynorm,ymean] = normRating(y,r)
    [m,n] = size(y);
    ymean = zeros(m,1);
    ynorm = zeros(size(y));

    y = y.*r ;
    ymean = mean(y,2);
    ynorm = y - ymean; 
end
[ynorm,ymean] = normRating(y,r);
num_users = size(y,2);
num_movies = size(y,1);
num_feat = 10;

x = randn(num_movies,num_feat);
theta = randn(num_users,num_feat);
init_params = [x(:); theta(:)];

options = optimset('GradObj','on','MaxIter',100);
lamda = 10;
theta = fmincg(@(t)(computeCost(t,ynorm,r,num_users,num_movies,num_feat,lamda)), ...
                init_params,options);
x = reshape(theta(1:num_movies * num_feat),num_movies,num_feat);
theta = reshape(theta(num_movies * num_feat +1 : end),num_users,num_feat);
fprintf('train over............\n');

% ============================ part 5 ===================
% 利用推荐系统推荐商品
p = x * theta'; %'
pre = p(:,1) + ymean;
movieList = loadMovieName();
[r,ix] = sort(pre,'descend');
fprintf('top 10:\n');
for i = 1:10
    j = ix(i);
    fprintf('%.1f for movie %s\n',pre(j),movieList{j});
end

fprintf('\n\nOriginal ratings provided:\n');
for i = 1:length(my_ratings)
    if my_ratings(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings(i), ...
                 movieList{i});
    end
end