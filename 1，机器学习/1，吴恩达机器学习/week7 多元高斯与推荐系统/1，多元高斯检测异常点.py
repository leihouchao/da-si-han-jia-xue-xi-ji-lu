import scipy.io as sio  
import matplotlib.pyplot as plt  
import numpy as np  
from numpy import *
from PIL import Image
import random 
import math

# 加载数据
def loadData(filename):
    data = sio.loadmat(filename);
    x = data['X'];
    xval = data['Xval'];
    yval = data['yval'];
    return x,xval,yval;
# 画图
def plotData(x):
    x = np.array(x);
    y = x[:,1];
    x = x[:,0];
    fig = plt.figure();
    ax = fig.add_subplot(111)
    ax.scatter(x,y,s = 10, c = 'g',marker = 'x')
    plt.show()
# 计算多元高斯每一维的 均值和方差
def computeMean(x):
    [m,n] = shape(x);
    mu = zeros((1,n))
    sigma = zeros((1,n))

    mu = mean(x,axis = 0)
    sigma = sum(pow(x - mu,2),axis = 0)/m;
    mu = reshape(mu,(1,n))
    sigma = reshape(sigma,(1,n))
    return mu,sigma;
# 计算每个样本的正常的概率
def multiGuassianCompute(x,mu,sigma):
    m,n = shape(x)
    tmp = [];
    for i in range(n):
        tmp.append(sigma[0][i])
        # sigma = [sigma[0][0],sigma[0][1]]
    sigma = diag(tmp);

    x = x - mu;
    x = mat(x); sigma = mat(sigma);
    p = pow(2*pi,-n/2) * pow(linalg.det(sigma),0.5) \
        * exp(-0.5 * sum(multiply(x * linalg.pinv(sigma),x),axis = 1))
    return p;
# 绘图: 画等高线
def visiualize(x,mu,sigma):
    # 画点
    x = np.array(x);
    y = x[:,1];
    x = x[:,0];
    fig = plt.figure();
    ax = fig.add_subplot(111)
    ax.scatter(x,y,s = 10, c = 'g',marker = 'x')

    # 画线
    x1 = np.arange(0,35,0.5);
    x2 = np.arange(0,35,0.5);
    [x1,x2] = meshgrid(x1,x2);
    m,n = shape(x1)
    x1 = reshape(x1,(m*n,1))
    x2 = reshape(x2,(m*n,1))
    
    # 整合成二维数组
    tmp = []
    for i in range(m*n):
        line = [x1[i][0], x2[i][0]]
        tmp.append(line)
    # 计算p值
    z = multiGuassianCompute(tmp,mu,sigma)
    x1 = reshape(x1,(m,n));
    x2 = reshape(x2,(m,n));
    z = reshape(z,(m,n));
    # 计算等高线的值
    tmp = np.arange(-20,0,3);
    tmp_num = []
    for i in range(len(tmp)):
        temp = math.pow(10,tmp[i]);
        tmp_num.append(temp);
    plt.contour(x1,x2,z,tmp_num);
    plt.show()
# 利用交叉验证选出最好的阈值
def selectThreshold(y,pval):
    bestEposion = 0;
    bestF1 = 0;
    F1 = 0;
    stepSize = (max(pval) - min(pval)) / 1000;
    for epo in arange(min(pval),max(pval),stepSize):
        pre = (pval < epo)
        tp = sum((pre == 1) & (y == 1))
        fp = sum((pre == 1) & (y == 0))
        fn = sum((pre == 0) & (y == 1))
        # 计算召回率和准确率
        if tp + fp != 0:
            prec = tp / (tp+fp)
        else:
            prec = 0
        if tp + fn != 0:
            rec = tp/(tp+fn)
        else:
            rec = 0
        
        # 计算F1 
        if prec != 0 and rec != 0:
            F1 = 2 * prec * rec / (prec + rec)
        else:
            F1 = 0
        if F1 > bestF1:
            bestF1 = F1 
            bestEposion = epo 
    return bestEposion,bestF1;   
# 画出异常点
def outlier(x,mu,sigma,tmp_x,tmp_y):
    # 画点
    x = np.array(x);
    y = x[:,1];
    x = x[:,0];
    fig = plt.figure();
    ax = fig.add_subplot(111)
    ax.scatter(x,y,s = 10, c = 'g',marker = 'x')

    # 画线
    x1 = np.arange(0,35,0.5);
    x2 = np.arange(0,35,0.5);
    [x1,x2] = meshgrid(x1,x2);
    m,n = shape(x1)
    x1 = reshape(x1,(m*n,1))
    x2 = reshape(x2,(m*n,1))
    
    # 整合成二维数组
    tmp = []
    for i in range(m*n):
        line = [x1[i][0], x2[i][0]]
        tmp.append(line)
    # 计算p值
    z = multiGuassianCompute(tmp,mu,sigma)
    x1 = reshape(x1,(m,n));
    x2 = reshape(x2,(m,n));
    z = reshape(z,(m,n));
    # 计算等高线的值
    tmp = np.arange(-20,0,3);
    tmp_num = []
    for i in range(len(tmp)):
        temp = math.pow(10,tmp[i]);
        tmp_num.append(temp);
    plt.contour(x1,x2,z,tmp_num);

    # 画异常点
    ax.scatter(tmp_x,tmp_y,c = 'red', marker = 'o')
    plt.show()

# 主函数
def main():
    x,xval,yval = loadData('multiGuassianData.mat');
    print(shape(x))
    print(shape(xval))
    print(shape(yval))
    # plotData(x);

    print('*****************************************')
    [mu,sigma] = computeMean(x)
    print(shape(mu))
    print(shape(sigma))
    print('mu = ',mu)
    print('sigma = ',sigma)
    p = multiGuassianCompute(x,mu,sigma)
    print(shape(p))
    
    print('*****************************************')
    visiualize(x,mu,sigma)

    print('*****************************************')
    pval = multiGuassianCompute(xval,mu,sigma);
    eposion,F1 = selectThreshold(yval,pval);
    print("epo = ",eposion)
    print('F1 = ',F1,' (0.875)')

    print('*****************************************')
    # 圈出异常点
    tmp_x = []; tmp_y = [];
    m,n = shape(x);
    for i in range(m):
        if pval[i] < eposion:
            tmp_x.append(x[i,0])
            tmp_y.append(x[i,1])
    outlier(x,mu,sigma,tmp_x,tmp_y)

    print('*****************************************')
    # 将高斯模型应用到高维
    x,xval,yval = loadData('multiGuassianData2.mat');
    [mu,sigma] = computeMean(x);
    p = multiGuassianCompute(x,mu,sigma)
    pval = multiGuassianCompute(xval,mu,sigma)
    epo,F1 = selectThreshold(yval,pval);
    print('epo = ', epo)
    print('f1 = ',F1,' (0.615385)')
    print('发现 ',sum(p < epo),'个异常点')

main()
