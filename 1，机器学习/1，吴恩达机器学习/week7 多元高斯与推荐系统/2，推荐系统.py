import scipy.io as sio  
import matplotlib.pyplot as plt  
import numpy as np  
from numpy import *
from PIL import Image
import random 
import math

# 读取数据
def loadData(filename):
    data = sio.loadmat(filename)
    y = data['Y']
    r = data['R']
    return y,r 
# 加载参数
def loadParams(filename):
    data = sio.loadmat(filename)
    theta = data['Theta']
    x = data['X']
    num_feats = data['num_features']
    num_movies = data['num_movies']
    num_users = data['num_users']
    return theta,x,num_feats,num_movies,num_users 
# 计算Cost与梯度
def computeCost(x,theta,y,r,num_users,num_movies,num_feat,lamda):
    x = mat(x); theta = mat(theta);
    J = 0
    x_grad = zeros((shape(x)))
    theta_grad = zeros((shape(theta)))

    # 评分矩阵
    errNum = multiply(x * theta.T - y,r)
    J = 0.5 * sum(sum(pow(np.array(errNum),2))) + lamda / 2 * sum(sum(pow(np.array(theta),2))) + \
        lamda/2*sum(sum(pow(np.array(x),2)))
    x_grad = errNum * theta + lamda *x 
    theta_grad = errNum.T * x + lamda * theta;

    grad = [x_grad[:], theta_grad[:]]
    return J,grad 
# 计算数值梯度
def computeNumGrad(x,theta,y,r,num_users,num_movies,num_feat,lamda):
    numgrad = zeros((shape(theta)))
    item = zeros((shape(theta)))
    e = 1e-4 
    for p in range(len(theta)):
        item[p] = e 
        loss1,_ = computeCost(x,theta - item,y,r,num_users,num_movies,num_feat,lamda);
        loss2,_ = computeCost(x,theta + item,y,r,num_users,num_movies,num_feat,lamda);
        numgrad[p] = (loss2 - loss1) / (2*e)
        item[p] = 0
    return numgrad
# 梯度校验
def checkGradient(lamda):
    x_t = reshape(np.random.rand(12),(4,3))
    theta_t = reshape(np.random.rand(15),(5,3))
    y = mat(x_t) * mat(theta_t).T 
    y[reshape(np.random.rand(20),(4,5)) > 0.5] = 0
    r = zeros((shape(y)))
    r[y != 0] = 1

    x =  reshape(np.random.rand(12),(4,3))
    theta =  reshape(np.random.rand(15),(5,3))
    num_users = shape(y)[1]
    num_movies = shape(y)[0]
    num_feat = shape(theta)[1]

    # 数值梯度
    numgrad = computeNumGrad(x,theta,y,r,num_users,num_movies,num_feat,lamda)
    cost,grad = computeCost(x,theta,y,r,num_users,num_movies,num_feat,lamda)
    print(shape(numgrad))
    print(shape(grad))
    # 显示计算结果
    # print(numgrad,grad)
    # diff = linalg.norm(numgrad - grad) / linalg.norm(numgrad + grad)
    # print('差距: ',diff)

# 主函数
def main():
    y,r = loadData('recommendData_movies.mat');
    m,n = shape(y)
    total = 0; num = 0;
    for i in range(n):
        if r[0][i] == 1:
            total += y[0][i]
            num += 1
    print('movie 1 scoer: ',total/num)
   
    print('********************************************')
    Y = y; R = r;
    Theta,X,num_feats,num_movies,num_users = loadParams('ex8_movieParams.mat')
    num_users = 4;
    num_movies = 5;
    num_feat = 3;
    x = X[0:num_movies,0:num_feat];
    theta = Theta[0:num_users,0:num_feat];
    y = Y[0:num_movies,0:num_users];
    r = R[0:num_movies,0:num_users];
    J,grad = computeCost(x,theta,y,r,num_users,num_movies,num_feat,0);
    print('J = ',J,' (22.22)')

    print('********************************************')
    checkGradient(0)

    print('********************************************')

    print('********************************************')

main()
